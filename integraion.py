from flask import make_response
from pony.orm import db_session
from model import InvoiceManager
from server import app
import xml.etree.ElementTree as ET


@app.route('/integration/1c/invoices', methods=['GET'])
@db_session
def one_s_invoices():
    invoice_list = InvoiceManager.find()
    root = ET.Element("root", {'Document': 'Order'})
    for invoice in invoice_list:
        if invoice.is_payed or invoice.is_owner:
            payment_order = ET.SubElement(root, 'PaymentOrder')
            num = ET.SubElement(payment_order, 'Num')
            num.text = str(invoice.id)
            date = ET.SubElement(payment_order, 'Date')
            date.text = str(invoice.created_at.isoformat())
            sum = ET.SubElement(payment_order, 'Sum')
            sum.text = str(invoice.price or 0)
            client = ET.SubElement(payment_order, 'Client')
            fio = ET.SubElement(client, 'FIO')
            fio.text = u"%s %s" % (invoice.person_lastname or '', invoice.person_firstname or '')
            email = ET.SubElement(client, 'Email')
            email.text = invoice.person_email
            phone = ET.SubElement(client, 'PhoneNum')
            phone.text = invoice.person_phone or ''
            is_owner = ET.SubElement(payment_order, 'IsOwner')
            is_owner.text = 'true' if invoice.is_owner else 'false'
            orders = ET.SubElement(payment_order, 'Orders')
            for booking in invoice.booking:
                order = ET.SubElement(orders, 'Order')
                order_id = ET.SubElement(order, 'ID')
                order_id.text = str(booking.id)
                order_date = ET.SubElement(order, 'Date')
                order_date.text = str(booking.created_at.isoformat())
                rent_date = ET.SubElement(order, 'RentDate')
                rent_date.text = str(booking.start.isoformat())
                return_date = ET.SubElement(order, 'ReturnDate')
                return_date.text = str(booking.end.isoformat())
                give_at_date = ET.SubElement(order, 'GiveAtDate')
                give_at_date.text = str(booking.give_at.isoformat()) if booking.give_at is not None else None
                return_at_date = ET.SubElement(order, 'ReturnAtDate')
                return_at_date.text = str(booking.return_at.isoformat()) if booking.return_at is not None else None
                item_id = ET.SubElement(order, 'NomenclatureArticle')
                item_id.text = str(booking.item.art)
                days = ET.SubElement(order, 'Days')
                days.text = str(booking.days)
                price = ET.SubElement(order, 'Price')
                price.text = str(booking.item.price)
                discount = ET.SubElement(order, 'Discount')
                discount.text = str(booking.discount)
                sum = ET.SubElement(order, 'Sum')
                sum.text = str(booking.price)
                deposit = ET.SubElement(order, 'Deposit')
                deposit.text = str(booking.item.deposit)
    xml_data = '<?xml version="1.0" encoding="UTF-8"?>%s' % ET.tostring(root)
    response = make_response(xml_data)
    response.headers["Content-Type"] = "application/xml"
    return response
