$(document).ready(function(){

    function set_items_hover(){
        var $itemscatalog = $('#items-catalog');

        $itemscatalog.find('.item-thumbnail').hover(
            function(){
                $(this).find('.item-caption').fadeIn(250);
            },
            function(){
                $(this).find('.item-caption').fadeOut(250);
            }
        );
    }

    $('#itemModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var id = button.data('id');
      var art = button.data('art');
      var img = button.data('img');
      var title = button.data('title');
      var price = button.data('price');
      var min_period = button.data('min_period');
      var deposit = button.data('deposit');
      var min_period_text = '';
      var deposit_text = '';
      var min_period_title = [];
        min_period_title[1] = 'week';
        min_period_title[2] = 'month';

      if (!id) {
          return;
      }

      var modal = $(this);
      modal.find('.modal-body .item-title').text(title);
      modal.find('.modal-body .item-id').text("SKU "+art);
      modal.find('.modal-body .item-img').attr("src", img);
      modal.find('.modal-body .item-price').text(price + ' euro per day');
      if (min_period && min_period != 0) {
        min_period_text = 'Minimum rental period &mdash; ' + min_period_title[min_period]
      }
      if (deposit != 0) {
          deposit_text = 'Deposit ' + deposit + ' euro'
      }
      modal.find('.modal-body .item-min-period').text(min_period_text);
      modal.find('.modal-body .item-deposit').text(deposit_text);
      modal.find('.modal-body .item-booking').text('');
      modal.find('.modal-body input[name="item_id"]').attr("value", id);

      $.getJSON($SCRIPT_ROOT + '/item/' + id, function( data ) {
        if (data.booking.length) {
            modal.find('.modal-body .item-booking').append($('<p>', {
                text: 'Item is occupied on following dates',
                class: 'secondary'
            }));
        } else {
            modal.find('.modal-body .item-booking').append($('<p>', {
                text: 'Vacant',
                class: 'secondary'
            }));
        }

        $.each(data.booking, function(index, element) {
          modal.find('.modal-body .item-booking').append($('<p>', {
            text: element.start + ' - ' + element.end
            }));
        });
      });

    });

    $( "#bookingForm" ).submit(function( event ) {

      // Stop form from submitting normally
      event.preventDefault();

      // Get some values from elements on the page:
      var $form = $( this ),
        item_id = $form.find( "input[name='item_id']" ).val(),
        start = $form.find( "input[name='start']" ).val(),
        end = $form.find( "input[name='end']" ).val(),
        url = $form.attr( "action" );

      // Send the data using post
      var posting = $.post( url, { item_id: item_id, start: start, end: end } );

      // Put the results in a div
      posting.done(function( data ) {
        $('#itemModal').modal('hide');
        window.location.href = $SCRIPT_ROOT + "/cart";
      });

      posting.fail(function( jqXHR, textStatus ) {
        $form.find("#item-errors").text(jqXHR.responseText);
        $form.find(".date-period").addClass('has-error');
      });
    });

    $('.input-group.date').datepicker({
        language: "en",
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        forceParse: true
    }).mask("9999-99-99");

    $('input.phone_number').mask("+7 (999) 999-99-99");

    $(function() {
        var form = $('form.filter');
        var container = $('#items-container');
        form.find("#show_all").click(function( event ) {
            event.preventDefault();
            form.find('input[type=checkbox]').attr('checked', false);
            $.ajax({
                type: "GET",
                url: form.action,
                success: function(msg){
                    container.html(msg);
                },
                error: function(){
                    console.log("filter failure");
                }
            });
        });
        form.find("input").change(function(){
            $.ajax({
            type: "GET",
            url: form.action,
            data: form.serialize(),
            success: function(msg){
                container.html(msg);
                set_items_hover();
                },
            error: function(){
                console.log("filter failure");
                }
            });
        });
    });

    $(".clickable").click(function() {
        window.location = $(this).data("location");
        return false;
    });

    set_items_hover();

});
