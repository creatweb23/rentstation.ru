step(
    """
    ALTER TABLE booking ADD COLUMN give_at timestamp(6);
    ALTER TABLE booking ADD COLUMN return_at timestamp(6);
    """
)
