step(
    """
    CREATE SEQUENCE invoice_id_seq;
    CREATE TABLE invoice (
        "id" int4 NOT NULL DEFAULT nextval('invoice_id_seq'),
        "person_lastname" varchar(255),
        "person_firstname" varchar(255),
        "person_email" varchar(255),
        "price" integer,
        "is_trilife" boolean default FALSE,
        "is_payed" boolean default FALSE,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "invoice_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE invoice_id_seq OWNED BY invoice.id;

    truncate table booking;

    alter table booking drop person_lastname;
    alter table booking drop person_firstname;
    alter table booking drop person_email;
    alter table booking drop price;
    alter table booking drop is_payed;
    alter table booking drop is_trilife;
    alter table booking add invoice_id int4 not null;
    ALTER TABLE booking ADD FOREIGN KEY(invoice_id) REFERENCES invoice(id);
    """
)