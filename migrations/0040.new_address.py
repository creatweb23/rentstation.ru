# coding=utf-8
step(
    """
    insert into address (title, address, code, city_title) VALUES ('Велотрек в Крылатском', 'г. Москва,\r\nКрылатская д. 10\r\nТелефон: +7 (495) 665-83-56\r\nВремя работы: 10.00-20.00', 'Velotrek', 'Москва, Велотрек в Крылатском');
    update address set city_title = 'Москва, Tri-sport на Земляном валу' where id = 2;
    update address set city_title = 'Москва, Велопробег на Дмитровке' where id = 1;
    update address set city_title = 'Санкт-Петербург, TRIATLETA' where id = 3;
    """
)