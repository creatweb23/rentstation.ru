step(
    """
    alter table booking add column person_email varchar not null default 'email';
    alter table booking alter column person_email drop default;
    """
)