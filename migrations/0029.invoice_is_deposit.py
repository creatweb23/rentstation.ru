step(
    """
    alter table invoice add column is_deposit boolean default TRUE;
    update invoice set is_deposit = TRUE;
    """
)