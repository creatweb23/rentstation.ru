step(
    """
    ALTER TABLE bag ADD COLUMN is_to_rent boolean default FALSE;
    ALTER TABLE bag ADD COLUMN is_to_sell boolean default FALSE;
    ALTER TABLE bag ADD COLUMN is_sold boolean default FALSE;
    """
)
