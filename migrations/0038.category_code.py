step(
    """
    ALTER TABLE category ADD COLUMN code varchar(255);
    UPDATE category set code = '92f9de58-7832-11e5-8386-001dd8b71ddd' where id = 1;
    UPDATE category set code = '92f9de5a-7832-11e5-8386-001dd8b71ddd' where id = 2;
    UPDATE category set code = '92f9de5d-7832-11e5-8386-001dd8b71ddd' where id = 5;
    UPDATE category set code = '92f9de5f-7832-11e5-8386-001dd8b71ddd' where id = 7;
    UPDATE category set code = '92f9de60-7832-11e5-8386-001dd8b71ddd' where id = 8;
    UPDATE category set code = '92f9de59-7832-11e5-8386-001dd8b71ddd' where id = 9;
    UPDATE category set code = '92f9de62-7832-11e5-8386-001dd8b71ddd' where id = 10;
    UPDATE category set code = '92f9de63-7832-11e5-8386-001dd8b71ddd' where id = 11;
    UPDATE category set code = '92f9de64-7832-11e5-8386-001dd8b71ddd' where id = 12;
    UPDATE category set code = '92f9de65-7832-11e5-8386-001dd8b71ddd' where id = 13;
    UPDATE category set code = '92f9de61-7832-11e5-8386-001dd8b71ddd' where id = 16;
    """
)
