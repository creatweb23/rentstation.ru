# coding=utf-8
step(
    """
    CREATE SEQUENCE act_template_id_seq;
    CREATE TABLE act_template (
        "id" int4 NOT NULL DEFAULT nextval('act_template_id_seq'),
        "title" varchar(255),
        "template" text,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "act_template_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE act_template_id_seq OWNED BY act_template.id;

    insert into act_template (title, template) values ('Стандартный', 'default');
    insert into act_template (title, template) values ('Для гидриков', 'wetsuit');

    ALTER TABLE bag ADD COLUMN "act_template" int4 NOT NULL default 1;
    """
)
