step(
    """
    alter table booking rename column person to person_lastname;
    alter table booking add column person_firstname varchar(255);
    update booking set person_firstname = 'Alex';
    alter table booking alter column person_firstname set not null;
    """
)