step(
    """
    ALTER TABLE bag ADD COLUMN deposit integer not null default 0;
    ALTER TABLE bag ADD COLUMN estimate integer not null default 0;
    """
)