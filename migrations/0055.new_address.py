step(
    """
    ALTER TABLE address ADD COLUMN url varchar(255);
    insert into address (title, address, code, city_title, city_id, ya_sid, url) VALUES ('Снежная белка в Яхроме', 'Яхрома\r\n64 км от Дмитровского шоссе\r\nВремя работы:\r\nПт-Сб с 10:00 до 04:00\r\nВс-Чт 10:00-24:00\r\nТелефон: +7 (916) 687-88-81', 'snowbelka', 'Яхрома, Снежная белка', 1, 'IHn8Ut2T_JYmryzhTR9ak5SrmAP8VghD', 'http://snowbelka.ru');
    update  address set url = 'http://www.veloprobeg.ru' where id = 1;
    update  address set url = 'http://tri-sport.ru' where id = 2;
    update  address set url = 'http://triatleta-shop.ru' where id = 3;
    update  address set url = 'http://serpantin-shop.ru' where id = 4;
    update  address set title = 'Серпантин в Крылатском' where id = 4;
    update  address set address = 'г. Москва\r\nКрылатская, 10\r\nв здании Велотрека, вход со стороны Большого пруда\r\nВремя работы: 10.00-20.00\r\nТелефон: +7 (925) 064-66-57' where id = 4;
    update  address set city_title = 'Москва, Серпантин в Крылатском' where id = 4;
    update  address set url = 'http://выбирайте.рф' where id = 5;
    update  address set url = 'http://ski.moscow' where id = 6;
    update  address set url = 'http://likeafish.ru' where id = 7;
    """
)
