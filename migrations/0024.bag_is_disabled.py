step(
    """
    ALTER TABLE bag ADD COLUMN is_disabled boolean default FALSE;
    """
)