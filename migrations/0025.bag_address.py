# coding=utf-8
step(
    """
    CREATE SEQUENCE address_id_seq;
    CREATE TABLE address (
        "id" int4 NOT NULL DEFAULT nextval('address_id_seq'),
        "title" varchar(255),
        "address" text,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "address_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE address_id_seq OWNED BY address.id;

    insert into address (title, address) values ('Велопробег на Дмитровке', 'г. Москва,\r\nм. Петровско-Разумовская, Дмитровское шоссе,\r\nдом 46, корпус 1\r\nТелефон: 8 (495) 540-58-57\r\nВремя работы магазина:\r\nПн-Пт: 11.00-21.00\r\nСб: 10.00-21.00\r\nВс: 10.00-18.00');
    insert into address (title, address) values ('Tri-sport на Балаклавском', 'г. Москва,\r\nм. Чертановская, Балаклавский проспект,\r\nдом 33 строение 5\r\nТелефон: 8 (499) 390-40-58\r\nВремя работы магазина:\r\n11.00-22.00\r\nПн. - выходной');

    ALTER TABLE bag ADD COLUMN "address" int4 NOT NULL default 1;
    """
)
