step(
    """
    insert into address (title, address, code, city_title, city_id, ya_sid) VALUES ('выбирайте.рф', 'г. Москва,\r\nАллея первой Маевки, д.15\r\nВремя работы:\r\nбудни с 9 часов до 23 часов\r\nвыходные с 9 часов до 23 часов\r\n(обязательно предварительно звоните)\r\nТелефон: +7 (929) 678-02-10', 'vybirayte', 'Москва, выбирайте.рф', 1, 'j0HLqtgtzEm7ox7v3JtvPjXrIoJwT4-3');
    insert into address (title, address, code, city_title, city_id, ya_sid) VALUES ('SKI.MOSCOW', 'г. Москва,\r\nул. Новая Басманная 31\r\nВремя работы:\r\nЕжедневно с 10:00 до 20:00\r\nТелефон: +7 (926) 555-34-55\r\nТелефон: +7 (499) 390-05-28', 'ski_moscow', 'Москва, SKI.MOSCOW', 1, 'keOWCvLeg7ZViHj1QPB2B13Jx20CRtTO');
    insert into address (title, address, code, city_title, city_id, ya_sid) VALUES ('LikeaFish', 'г. Москва,\r\nСиреневый бульвар 2, стр. 1Б\r\nВремя работы:\r\nЕжедневно с 10:00 до 18:00\r\nПредварительно звоните!\r\nТелефон: +7 (968) 955-55-54', 'likeafish', 'Москва, LikeaFish', 1, 'FDQGCX-2l-K7oN8HLXpkBzySey1Nq-wV');
    """
)
