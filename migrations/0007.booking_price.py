step(
    """
    alter table booking add column price int not null default 0;
    alter table booking alter column price DROP DEFAULT;
    """
)