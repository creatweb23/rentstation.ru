# coding=utf-8
step(
    """
    ALTER TABLE address ADD COLUMN city_title varchar(255);
    update  address set city_title = 'Москва' where id in (1,2);
    update  address set city_title = 'Санкт-Петербург' where id = 3;
    """
)