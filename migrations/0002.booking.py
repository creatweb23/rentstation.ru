step(
    """
    CREATE SEQUENCE booking_id_seq;
    CREATE TABLE booking (
        "id" int4 NOT NULL DEFAULT nextval('booking_id_seq'),
        "bag_id" int4 NOT NULL,
        "start" timestamp(6) NOT NULL,
        "end" timestamp(6) NOT NULL,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "booking_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE booking_id_seq OWNED BY booking.id;
    ALTER TABLE booking ADD FOREIGN KEY(bag_id) REFERENCES bag(id);
    """
)