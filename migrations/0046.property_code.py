step(
    """
    ALTER TABLE property ADD COLUMN code varchar(255);
    ALTER TABLE option ADD COLUMN code varchar(255);
    """
)
