# coding=utf-8
step(
    """
    CREATE SEQUENCE property_id_seq;
    CREATE TABLE property (
        "id" int4 NOT NULL DEFAULT nextval('property_id_seq'),
        "title" varchar(255),
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "property_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE property_id_seq OWNED BY property.id;

    CREATE SEQUENCE option_id_seq;
    CREATE TABLE option (
        "id" int4 NOT NULL DEFAULT nextval('option_id_seq'),
        "title" varchar(255),
        "property_id" int4 NOT NULL,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "option_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE option_id_seq OWNED BY option.id;

    CREATE SEQUENCE bag_option_id_seq;
    CREATE TABLE bag_option (
        "id" int4 NOT NULL DEFAULT nextval('bag_option_id_seq'),
        "item_id" int4 NOT NULL,
        "option_id" int4 NOT NULL,
        CONSTRAINT "bag_option_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE bag_option_id_seq OWNED BY bag_option.id;
    """
)
