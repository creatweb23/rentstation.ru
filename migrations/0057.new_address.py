step(
    """
    UPDATE address set title = 'Прокат в Симферополе' where id = 10;
    UPDATE address set address = 'Крым, Симферопольский р-н, с. Строгоновка, пер. Озенбаш, д. 1А\r\nВремя работы:\r\n10:00 до 19:00\r\nТелефон: +7 (926) 219-99-37' where id = 10;
    UPDATE address set city_title = 'Крым, прокат в Симферополе' where id = 10;
    """
)
