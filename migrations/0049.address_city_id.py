# coding=utf-8
step(
    """
    ALTER TABLE address ADD COLUMN city_id int4;
    update  address set city_id = 1 where id in (1,2,4);
    update  address set city_id = 2 where id = 3;
    """
)