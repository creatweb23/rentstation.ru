step(
    """
    ALTER TABLE bag DROP COLUMN category;
    CREATE SEQUENCE category_id_seq;
    create table category (
      "id" int4 NOT NULL DEFAULT nextval('category_id_seq'),
      "title" varchar(255) NOT NULL,
      "weight" int4 NOT NULL DEFAULT 0,
      CONSTRAINT "category_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE category_id_seq OWNED BY category.id;

    create table bag_category (
      "item_id" int4 NOT NULL,
      "category_id" int4 NOT NULL
    )
    WITH (OIDS=FALSE);

    insert into category (title, weight) values ('Триатлон', 2000);
    insert into category (title, weight) values ('Гольф', 1900);
    insert into category (title, weight) values ('Хоккей', 1800);
    insert into category (title, weight) values ('Скейт', 1700);
    insert into category (title, weight) values ('Кайт/Вейк', 1600);
    insert into category (title, weight) values ('Конный спорт', 1500);
    insert into category (title, weight) values ('Сноуборд/Горные лыжи', 1400);
    insert into category (title, weight) values ('Путешествия', 1300);
    insert into category (title, weight) values ('Велоспорт', 1200);
    insert into category (title, weight) values ('Дайвинг/подводная рыбалка', 1100);
    insert into category (title, weight) values ('Пейнтбол/Страйкбол', 1000);
    """
)