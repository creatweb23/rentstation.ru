step(
    """
    CREATE SEQUENCE bag_id_seq;
    CREATE TABLE bag (
        "id" int4 NOT NULL DEFAULT nextval('bag_id_seq'),
        "title" varchar(255) NOT NULL,
        "image" varchar(255) NOT NULL,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "bag_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE bag_id_seq OWNED BY bag.id;
    """
)