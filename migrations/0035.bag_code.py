step(
    """
    ALTER TABLE bag ADD COLUMN code varchar(255);
    ALTER TABLE address ADD COLUMN code varchar(255);
    UPDATE address set code = 'Trisport' where id = 2;
    UPDATE address set code = 'Veloprobeg' where id = 1;
    """
)
