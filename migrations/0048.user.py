step(
    """
    CREATE SEQUENCE client_id_seq;
    CREATE TABLE client (
        "id" int4 NOT NULL DEFAULT nextval('client_id_seq'),
        "name" varchar(255) NOT NULL,
        "email" varchar(255) NOT NULL,
        "facebook_id" varchar(255) NOT NULL,
        "facebook_token" varchar(255) NOT NULL,
        "created_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "client_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE client_id_seq OWNED BY client.id;

    CREATE SEQUENCE balance_id_seq;
    CREATE TABLE balance (
        "id" int4 NOT NULL DEFAULT nextval('balance_id_seq'),
        "client_id" int4 NOT NULL,
        "balance" int4 NOT NULL,
        "updated_at" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
        CONSTRAINT "balance_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
    )
    WITH (OIDS=FALSE);
    ALTER SEQUENCE balance_id_seq OWNED BY balance.id;
    """
)

