step(
    """
    ALTER TABLE bag ADD COLUMN sell_price integer not null default 0;
    """
)
