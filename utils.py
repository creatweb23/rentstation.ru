# coding=utf-8
import urllib2

from flask import render_template
from flask.ext.mail import Message


def mail_payed(email, invoice):
    msg = Message(u'Ваш заказ на RentStation', recipients=[email])
    msg.sender = ('RentStation', 'info@rentstation.ru')
    msg.body = render_template('email/invoice.txt', invoice=invoice)
    return msg


def mail_confirmed(email, invoice):
    msg = Message(u'Ваш заказ на RentStation', recipients=[email])
    msg.sender = ('RentStation', 'info@rentstation.ru')
    msg.body = render_template('email/invoice_confirmed.txt', invoice=invoice)
    return msg


def mail_notify(invoice):
    email = 'info@rentstation.ru'
    msg = Message(u'Заказ на RentStation', recipients=[email])
    msg.sender = ('RentStation', 'info@rentstation.ru')
    msg.body = render_template('email/notify.txt', invoice=invoice)
    return msg


def get_url_contents(url):
    file_data = urllib2.urlopen(url)
    data = file_data.read()
    file_data.close()
    return data