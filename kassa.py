# coding=utf-8
import hashlib
import datetime
from flask import request, render_template
from pony.orm import db_session, commit
import xml.etree.ElementTree as ET
from model import Invoice
from server import app, mail
from utils import mail_payed, mail_notify

hash_keys = ['action', 'orderSumAmount', 'orderSumCurrencyPaycash', 'orderSumBankPaycash', 'shopId', 'invoiceId',
             'customerNumber']


def md5(s):
    return hashlib.md5(s).hexdigest()


def kassa_check_invoice(data):
    key = ';'.join([data[k] for k in hash_keys]) + ';%s' % app.config['KASSA_PASSWORD']
    if data['orderSumCurrencyPaycash'] == 643:
        code = 100
    elif str(data['md5']).lower() != str(md5(key)).lower():
        code = 1
    elif data['shopId'] == app.config['KASSA_SHOP_ID']:
        invoice = Invoice.get(id=int(data['orderNumber']))
        if float(invoice.price) == float(data['orderSumAmount']):
            code = 0
        else:
            code = 100
    else:
        code = 200

    message = ''
    tech_message = ''

    return {'performedDatetime': str(datetime.datetime.now().isoformat()),
            'code': str(code),
            'invoiceId': str(data['invoiceId']),
            'orderSumAmount': str(data['orderSumAmount']),
            'shopId': str(app.config['KASSA_SHOP_ID']),
            'message': message,
            'tech_message': tech_message}


@app.route('/demokassa/check', methods=['POST'])
@app.route('/kassa/check', methods=['POST'])
@db_session
def kassa_check_resource():
    check_data = kassa_check_invoice(request.form)
    root = ET.Element("checkOrderResponse", check_data)
    xml_data = '<?xml version="1.0" encoding="UTF-8"?>%s' % ET.tostring(root)
    return xml_data, 200


@app.route('/demokassa/aviso', methods=['POST'])
@app.route('/kassa/aviso', methods=['POST'])
@db_session
def kassa_aviso_resource():
    check_data = kassa_check_invoice(request.form)
    if check_data['code'] == '0':
        invoice = Invoice.get(id=int(request.form['orderNumber']))
        invoice.is_payed = True
        commit()
        for msg in [mail_payed(invoice.person_email, invoice), mail_notify(invoice)]:
            mail.send(msg)
    root = ET.Element("paymentAvisoResponse", check_data)
    xml_data = '<?xml version="1.0" encoding="UTF-8"?>%s' % ET.tostring(root)
    return xml_data, 200


@app.route('/demokassa/success')
@app.route('/kassa/success')
def kassa_success():
    return render_template('success.html')


@app.route('/demokassa/fail')
@app.route('/kassa/fail')
def kassa_fail():
    return render_template('fail.html')
