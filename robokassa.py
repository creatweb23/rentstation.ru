# coding=utf-8
from wtforms import Form, validators, HiddenField


class RobokassaForm(Form):
    MrchLogin = HiddenField(validators=[validators.required])
    OutSum = HiddenField(validators=[validators.required])
    InvId = HiddenField(validators=[validators.required])
    Desc = HiddenField(validators=[validators.required])
    Email = HiddenField(validators=[validators.required])
    Culture = HiddenField(validators=[validators.required])
