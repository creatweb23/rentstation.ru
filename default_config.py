import os

DATABASE = "postgres://pachay:bikebag@localhost/bikebag_net"
UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/static/uploads/'
UPLOAD_PATH = 'uploads/'
UPLOAD_SIZE = (400, 300)
DEBUG = False
SECRET_KEY = 'development key'
USERS = {
    'admin': 'default',
    'veloprobeg': 'veloprobeg',
    'trisport': 'trisport'
}

MAIL_DEBUG = False
MAIL_SERVER = 'smtp.yandex.ru'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'info@rentstation.net'
MAIL_PASSWORD = ''
DEFAULT_MAIL_SENDER = 'RentStation <info@rentstation.net>'

LOCAL = False

ROBOKASSA_LOGIN = 'rentstation'
ROBOKASSA_PASSWORD1 = ''
ROBOKASSA_PASSWORD2 = ''
ROBOKASSA_TEST_MODE = False

ROBOKASSA_FORM_TARGET = u'https://merchant.roboxchange.com/Index.aspx'
if ROBOKASSA_TEST_MODE:
    ROBOKASSA_FORM_TARGET = u'http://test.robokassa.ru/Index.aspx'

KASSA_SHOP_ID = 0
KASSA_SC_ID = 0
YANDEXKASSA_TEST_MODE = False
YANDEXKASSA_FORM_TARGET = u'https://money.yandex.ru/eshop.xml'
if YANDEXKASSA_TEST_MODE:
    YANDEXKASSA_FORM_TARGET = u'https://demomoney.yandex.ru/eshop.xml'

FACEBOOK_APP_ID = '000000000000000'
FACEBOOK_APP_SECRET = '0a0b0c00000000000000000000x0y0z0'
