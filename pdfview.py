# coding=utf-8
from contextlib import closing
from cStringIO import StringIO
from os.path import join, dirname
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase import ttfonts
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.platypus import Table, TableStyle

font_path = join(dirname(__file__), "fonts")
ArialFontObject = ttfonts.TTFont('Arial', join(font_path, 'arial.ttf'))
pdfmetrics.registerFont(ArialFontObject)
ArialFontObject = ttfonts.TTFont('Arial-Bold', join(font_path, 'arialbold.ttf'))
pdfmetrics.registerFont(ArialFontObject)

width, height = A4


def coord(x, y, unit=1):
    x, y = x * unit, height - y * unit
    return x, y


def invoice_pdf_view(booking_number, person, phone, email, title, item_id, estimate, deposit, start, end, period,
                     price, template, is_deposit):
    with closing(StringIO()) as buffer_string:
        c = canvas.Canvas(buffer_string, (width, height))

        def draw_annotation(invoice_id):
            c.setFont('Arial-Bold', 10)
            c.drawCentredString(width / 2, top_y, u"Приложение № %s" % invoice_id)
            c.setFont('Arial', 10)
            c.drawCentredString(width / 2, top_y - cm / 2,
                                u"к ПУБЛИЧНОМУ ДОГОВОРУ-ОФЕРТЕ НА ПРОКАТ СПОРТИВНОГО ОБОРУДОВАНИЯ, ")
            c.drawCentredString(width / 2, top_y - cm, u"размещенному на сайте WWW.RENTSTATION.RU")
            c.setFont('Arial-Bold', 10)
            c.drawCentredString(width / 2, top_y - 2 * cm, u"Акт приема-передачи")

        def draw_agreement(person):
            c.setFont('Arial', 10)
            c.drawString(top_x, top_y - 3 * cm,
                         u"Я, %s, паспорт серия _______ номер __________ выдан ___________________________" % person)
            c.drawString(top_x, top_y - 4 * cm,
                         u"________________________________________________________(кем) _________________(когда)")

        def draw_contacts(phone, email):
            c.setFont('Arial', 10)
            c.drawString(top_x, top_y - 4 * cm - cm / 2, u"телефон: %s, эл. почта: %s" % (phone, email))
            c.drawString(top_x, top_y - 5 * cm - cm / 2,
                         u"зарегистрирован(а) по адресу: _________________________________________________________")

        def draw_subject(title, item_id, estimate, start, end, period, deposit, price, is_deposit, template):
            c.setFont('Arial', 10)
            c.drawString(top_x, top_y - 6 * cm,
                         u"выступая в роли Арендатора согласно ПУБЛИЧНОМУ ДОГОВОРУ-ОФЕРТЕ НА ПРОКАТ")
            c.drawString(top_x, top_y - 6 * cm - cm / 2, u'СПОРТИВНОГО ОБОРУДОВАНИЯ, принимаю %s' % title)
            c.drawString(top_x, top_y - 7 * cm, u'артикул %s' % item_id)

            y_offset = 0
            if template == 'bike':
                c.drawString(top_x+180, top_y - 8 * cm, u'Диагностическая карта')
                data = [
                    [u'', u'Приемка арендатором', u'Возврат'],
                    [u'Чистота велосипеда \n(нужна ли мойка, смазка, \nотчистка от посторонних наклеек, предметов)', u'',
                     u''],
                    [u'Обмотка руля \n(повреждения, потертости, загрязнения, износ)', u'', u''],
                    [u'Переключение передач \n(четкость работы, \nцарапины и повреждения на преключателях, \n'
                     u'смазка и чистота цепи и звездочек)', u'', u''],
                    [u'Переднее колесо \n("восьмерки", давление воздуха, \nцарапины и вмятины на ободе, \n'
                     u'повреждение покрышки, целостность спиц)', u'', u''],
                    [u'Заднее колесо \n(«восьмерки», давление воздуха, \nцарапины и вмятины на ободе, \n'
                     u'повреждение покрышки, целостность спиц)', u'', u''],
                    [u'Тормозные колодки \n(степень износа, правильность установки)', u'', u''],
                    [u'Седло \n(чистота, наличие потертостей и царапин, наклеек)', u'', u''],
                    [u'Шатуны \n(состояние внутренней резьбы, \nналичие вмятин и царапин)', u'', u''],
                    [u'Рама \n(наличие сколов, повреждений рамы, \nперьев, выноса, руля, подседельного штыря, \n'
                     u'наличие наклеек, чистота)', u'', u''],
                    [u'Дополнительное оборудование \n(педали, флягодержатели, фляги, датчики и пр.)', u'', u'']
                ]
                t = Table(data, colWidths=[250, 120, 120])
                t.setStyle(TableStyle([
                    ('FONT', (0, 0), (-1, -1), 'Arial'),
                    ('ALIGN', (0, 0), (-1, 0), 'CENTER'),
                    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                    ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
                ]))
                t.wrapOn(c, width, height)
                t.drawOn(c, top_x, top_y - 24 * cm)
                y_offset = 17 * cm

            else:
                c.drawString(top_x, top_y - y_offset - 7 * cm - cm / 2,
                             u'Подтверждаю, что оборудование и комплектующие находятся в полностью исправном и целом')
                c.drawString(top_x, top_y - y_offset - 8 * cm,
                             u'состоянии, трещин и др. повреждений на корпусе нет, все комплектующие в полном комплекте')
                c.drawString(top_x, top_y - y_offset - 8 * cm - cm / 2,
                             u'и исправны, имеются/не имеются наружные повреждения,потертости (ненужное зачеркнуть).')
            if template == 'bike':
                c.showPage()
                c.setFont('Arial-Bold', 10)
                y_offset = 0
                c.drawString(top_x, top_y - cm, u"Штрафы:")
                c.drawString(top_x, top_y - 2 *cm,
                             u"За возврат велосипеда, требующего мойку, "
                             u"очистку от наклеек и посторонних предметов - 1500 руб.")
                c.drawString(top_x, top_y - 3 * cm,
                             u"Повреждения рамы, колес, навесного оборудования, обмотки руля, системы, сидения")
                c.drawString(top_x, top_y - 4 * cm,
                             u"и прочих деталей велосипеда - согласно оценке работника, ответственного за приемку")
                c.drawString(top_x, top_y - 5 * cm,
                             u"(стоимость работы + запчастей).")
                c.drawString(top_x, top_y - 6 * cm,
                             u"За каждый день просрочки после даты возврата взимается плата в 3-х кратном размере.")
                c.drawString(top_x, top_y - 7 * cm,
                             u"Со штрафами ознакомлен ____________ (_________________) «___» __________ 2016 г.")

            c.setFont('Arial', 10)
            c.drawString(top_x, top_y - y_offset - 9 * cm - cm / 2,
                         u'Дополнительная информация:  _________________________________________________________')
            c.drawString(top_x, top_y - y_offset - 10 * cm - cm / 2,
                         u'____________________________________________________________________________________')
            c.drawString(top_x, top_y - y_offset - 11 * cm - cm / 2,
                         u'____________________________________________________________________________________')
            c.drawString(top_x, top_y - y_offset - 12 * cm, u'Оценочная стоимость %s руб.' % estimate)
            c.drawString(top_x, top_y - y_offset - 12 * cm - cm / 2, u'на срок с %s по %s, %s дней' % (start, end, period))
            c.drawString(top_x, top_y - y_offset - 13 * cm, u'Стоимость аренды за указанный срок %s руб.' % price)
            if is_deposit:
                c.drawString(top_x, top_y - y_offset - 13 * cm - cm / 2, u'Вношу залог %s руб.' % deposit)
            else:
                c.drawString(top_x, top_y - y_offset - 13 * cm - cm / 2, u'Без залога.')
            c.drawString(top_x, top_y - y_offset - 14 * cm,
                         u"и даю согласие на обработку указанных выше персональных данных.")

        def draw_wetsuit_warn():
            y_offset = 0
            c.drawString(top_x, top_y - y_offset - 14 * cm - cm / 2,
                         u'Осведомлен(а) о штрафах за повреждение гидрокостюма:')
            c.drawString(top_x, top_y - y_offset - 15 * cm,
                         u'Сквозное повреждение - 5000 руб., несквозное - 2000 руб., мелкие, несквозные до 1см - 0 руб.')

        def draw_signature(is_deposit):
            y_offset = 0
            c.setFont('Arial', 10)
            c.drawString(top_x, top_y - y_offset - 16 * cm,
                         u'_________________________ (________________)      «___» __________ 2016 г.')
            if is_deposit:
                c.drawString(top_x, top_y - y_offset - 16 * cm - cm / 2, u'Оборудование выдано, залог и оплата получены')
            else:
                c.drawString(top_x, top_y - y_offset - 16 * cm - cm / 2, u'Оборудование выдано, оплата получена')
            c.drawString(top_x, top_y - y_offset - 17 * cm - cm / 2,
                         u'_________________________ (________________)      «___» __________ 2016 г.')

        def draw_spacer():
            c.setFont('Arial', 10)
            y_offset = 0
            c.drawString(top_x, top_y - y_offset - 18 * cm - cm / 2,
                         u"...................................................................................."
                         u".....................................................................................")

        def draw_return(title, is_deposit):
            y_offset = 0
            c.setFont('Arial', 10)
            c.drawString(top_x, top_y - y_offset - 19 * cm - cm / 2,
                         u'Я, _____________________________________________________, выступая в роли Арендодателя')
            c.drawString(top_x, top_y - y_offset - 20 * cm,
                         u'согласно ПУБЛИЧНОМУ ДОГОВОРУ-ОФЕРТЕ НА ПРОКАТ СПОРТИВНОГО ОБОРУДОВАНИЯ,')
            c.drawString(top_x, top_y - y_offset - 20 * cm - cm / 2, u'принимаю %s' % title)
            c.drawString(top_x, top_y - y_offset - 21 * cm, u'в состоянии и комплектации, описанной выше.')
            c.drawString(top_x, top_y - y_offset - 21 * cm - cm / 2, u'Претензий по состоянию и комплектации к Арендатору не имею.')
            if is_deposit:
                c.drawString(top_x, top_y - y_offset - 22 * cm - cm / 2, u'Залог возвращен в размере _________________ руб.')
            c.drawString(top_x, top_y - y_offset - 23 * cm - cm / 2,
                         u"Примечание _________________________________________________________________________")
            c.drawString(top_x, top_y - y_offset - 24 * cm - cm / 2,
                         u'____________________________________________________________________________________')
            if is_deposit:
                c.drawString(top_x, top_y - y_offset - 25 * cm - cm / 2,
                             u'Залог вернул _____________________ (_________________)      «___» __________ 2016 г.')
                c.drawString(top_x, top_y - y_offset - 26 * cm - cm / 2,
                             u'Залог получил ____________________ (_________________)      «___» __________ 2016 г.')

        top_x = 60
        top_y = height - 40

        c.setFont('Arial', 10)
        draw_annotation(booking_number)
        draw_agreement(person)
        draw_contacts(phone, email)
        draw_subject(title, item_id, estimate, start, end, period, deposit, price, is_deposit, template)
        if template == 'wetsuit':
            draw_wetsuit_warn()
        draw_signature(is_deposit)
        draw_spacer()
        draw_return(title, is_deposit)
        c.showPage()
        c.save()
        return buffer_string.getvalue()
