# -*- coding: utf-8 -*-
import StringIO
import re
import urllib2
import xml
from flask.ext.script import Manager
from pony.orm import db_session, commit, select
from slugify import slugify
from werkzeug.datastructures import MultiDict
from server import app
from model import BookingManager, Bag, BagForm, ActTemplateManager, Address, AddressManager, Category, CategoryManager, \
    ActTemplate, Property, PropertyForm, OptionForm, Option, OptionManager
from admin import make_thumb
from utils import get_url_contents

manager = Manager(app)


@manager.command
@db_session
def update_bookings():
    bookings = BookingManager.find()
    for booking in bookings:
        if booking.confirm_price is None:
            print 'updating booking %s' % booking.id
            try:
                booking.before_insert()
                booking.confirm_price = booking.price
                commit()
                print 'booking %s updated, confirm_price is %s' % (booking.id, booking.price)
            except:
                print 'error updating booking %s' % booking.id


def cast_price(var):
    if var is None:
        return None
    else:
        return str(re.sub('[^0-9]+', '', var))


def load_items(items):
    for atype in items:
        group_code = atype.find('NomenclatureGroup_ID').text.strip()
        # group_name = atype.text
        # if isinstance(group_name, basestring):
        #     group_name = group_name.strip()
        objs = atype.findall('obj')
        for obj in objs:
            # continue in case if article is missed
            if obj.find('Code') is None or obj.find('Code').text is None:
                print "Ignoring obj, Code missed"
                continue
            code = obj.find('Code').text.strip()

            if obj.find('Article') is None or obj.find('Article').text is None:
                print "Ignoring obj %s, Article not set" % code
                continue

            art = int(obj.find('Article').text.strip())
            title = obj.find('FullName').text
            description = obj.find('Description').text
            price = cast_price(obj.find('Price').find('day').text if obj.find('Price').find('day') is not None else None)
            deposit = cast_price(obj.find('Deposit').text)
            estimate = cast_price(obj.find('AccountPrice').text)
            sell_price = cast_price(obj.find('SalesPrice').text)
            address_code = obj.find('Location').find('Location_ID').text
            min_period_code = obj.find('MinPeriod_ID').text
            segments = obj.find('Segments').findall('Segment')
            options = obj.find('Options').findall('Option')
            is_to_rent_data = obj.find('ForRent').text
            is_to_sell_data = obj.find('ForSale').text
            is_sold_data = obj.find('Sold').text

            # find item with article
            item = Bag.get(art=art)
            if item is None:
                # try to find by code
                item = Bag.get(code=code)

            if description is None:
                print "Ignoring obj %s, description missed" % code
                continue

            if address_code is None:
                print "Ignoring obj %s, address code is None" % code
                if item:
                    item.delete()
                continue

            category_ids = []
            for segment in segments:
                segment_id = segment.find('Segment_ID').text
                category = Category.get(code=segment_id)
                if category is None:
                    print "Warning: segment %s not found" % segment_id
                    continue
                category_ids.append(category.id)

            address = Address.get(code=address_code)

            if address is None:
                print "Ignoring obj %s, address %s not found" % (code, address_code)
                continue

            min_period_dict = {
                'day': 0,
                'week': 1,
                'month': 2,
                'hour': 3
            }
            # min_period_dict = {
            #     '3f415980-68ad-4df1-ab3a-84649cba0205': 0,
            #     'f33bde66-d662-4d1a-bde1-8059684e729d': 1,
            #     'be662096-6ebd-4f9b-b1d2-70b55399f968': 2
            # }

            min_period = min_period_dict.get(min_period_code)

            if min_period is None:
                print "Ignoring obj %s, min_period %s not found" % (code, min_period_code)
                continue

            if price:
                price = price.translate(None, ',.')
                price = re.sub(r"\s+", "", price, flags=re.UNICODE)
                price = int(price)
            else:
                price = 0

            if sell_price:
                sell_price = sell_price.translate(None, ',.')
                sell_price = re.sub(r"\s+", "", sell_price, flags=re.UNICODE)
                sell_price = int(sell_price)
            else:
                sell_price = 0

            is_to_rent = False
            is_to_sell = False
            is_sold = False
            if is_to_rent_data == 'true':
                is_to_rent = True
            if is_to_sell_data == 'true':
                is_to_sell = True
            if is_sold_data == 'true':
                is_sold = True

            if is_to_rent and price == 0:
                print "Ignoring obj %s, is_to_rent is %s and price is 0" % (code, is_to_rent)
                continue

            if is_to_sell and sell_price == 0:
                is_to_sell = False
                print "Set is_to_sell to False, obj %s, is_to_sell is %s and sell_price is 0" % (code, is_to_sell)
                # print "Ignoring obj %s, is_to_sell is %s and sell_price is 0" % (code, is_to_sell)
                # continue

            if deposit:
                deposit = deposit.translate(None, ',.')
                deposit = re.sub(r"\s+", "", deposit, flags=re.UNICODE)
                deposit = int(deposit)
            else:
                deposit = 0

            if estimate:
                estimate = estimate.translate(None, ',.')
                estimate = re.sub(r"\s+", "", estimate, flags=re.UNICODE)
                estimate = int(estimate)
            else:
                estimate = 0

            pictures = [p.text.replace('\\', '/') for p in obj.find('Pictures')]
            picture = None
            if len(pictures) > 0:
                picture = pictures[0]

            if picture is None:
                print "Ignoring obj %s, image missed" % code
                continue

            act_template = ActTemplate.get(code=group_code)
            if act_template is None:
                act_template_id = 1
            else:
                act_template_id = act_template.id

            option_ids = []
            for option in options:
                option_code = option.find('Option_ID').text
                option_value = option.find('Option_Value_ID').text

                if option_value == 'true':
                    option_obj = Option.get(code=option_code)
                    if option_obj is not None:
                        option_ids.append(option_obj.id)

            data_list = [('code', code),
                         ('title', title),
                         ('description', description),
                         ('price', price),
                         ('deposit', deposit),
                         ('estimate', estimate),
                         ('image', picture),
                         ('act_template_id', act_template_id),
                         ('min_period', min_period),
                         ('address_id', str(address.id)),
                         ('sell_price', sell_price)] + \
                        [('category', cid) for cid in category_ids] + \
                        [('option', oid) for oid in option_ids]

            if is_to_rent:
                data_list.append(('is_to_rent', 'on'))
            if is_to_sell:
                data_list.append(('is_to_sell', 'on'))
            if is_sold:
                data_list.append(('is_sold', 'on'))
            data = MultiDict(data_list)

            form = BagForm(data)
            form.act_template_id.choices = [(str(i.id), i.title) for i in ActTemplateManager.find()]
            form.address_id.choices = [(str(i.id), i.title) for i in AddressManager.find()]
            form.category.choices = [(str(i.id), i.title) for i in CategoryManager.find()]
            form.option.choices = [(str(i.id), i.title) for i in OptionManager.find()]

            if not form.validate():
                print 'Ignoring obj %s, item is invalid: %s' % (code, form.errors)
                continue

            # update if found
            if item is not None:
                print 'updating item %s' % code
                address = Address.get(id=int(form.address_id.data))
                act_template = ActTemplateManager.get(item_id=map(int, form.act_template_id.data))
                categories = CategoryManager.find(item_id=map(int, form.category.data))
                if len(form.option.data) == 0:
                    options = []
                else:
                    options = OptionManager.find(ids=map(int, form.option.data))
                item.art = art
                item.title = form.title.data
                item.slug = "%s-%s" % (slugify(form.title.data, to_lower=True), art)
                item.address = address.id
                item.price = form.price.data
                item.sell_price = form.sell_price.data
                item.deposit = form.deposit.data
                item.estimate = form.estimate.data
                item.description = form.description.data
                item.min_period = form.min_period.data
                item.category = categories
                item.option = options
                item.act_template = act_template
                item.code = code
                item.is_to_rent = form.is_to_rent.data
                item.is_to_sell = form.is_to_sell.data
                item.is_sold = form.is_sold.data
                if item.image_code != picture:
                    response = urllib2.urlopen("http://d24.itliteclient.ru/1cexp/%s" % picture)
                    output = StringIO.StringIO()
                    output.write(response.read())
                    image_name = make_thumb(output)
                    item.image = image_name
                    item.image_code = picture
                    print 'image uploaded %s' % picture
                commit()
                print 'updated %s' % item.id
                continue

            # create if not found
            if item is None:
                print 'creating item'
                image_name = None
                if picture is not None:
                    response = urllib2.urlopen("http://d24.itliteclient.ru/1cexp/%s" % picture)
                    output = StringIO.StringIO()
                    output.write(response.read())
                    image_name = make_thumb(output)
                address = Address.get(id=int(form.address_id.data))
                categories = CategoryManager.find(item_id=map(int, form.category.data))
                options = OptionManager.find(ids=map(int, form.option.data))
                act_template = ActTemplateManager.get(item_id=map(int, form.act_template_id.data))
                bag_id = Bag(title=form.title.data,
                             slug="%s-%s" % (slugify(form.title.data, to_lower=True), art),
                             price=form.price.data,
                             sell_price=form.sell_price.data,
                             deposit=form.deposit.data,
                             estimate=form.estimate.data,
                             image=image_name,
                             description=form.description.data,
                             address=address.id,
                             act_template=act_template.id,
                             min_period=form.min_period.data,
                             image_code=picture,
                             code=code,
                             art=art,
                             category=categories,
                             option=options,
                             is_to_rent=form.is_to_rent.data,
                             is_to_sell=form.is_to_sell.data,
                             is_sold=form.is_sold.data,
                             )
                commit()
                print 'created %s' % bag_id
                continue


def load_options(opts):
    group_codes = []
    for opt in opts:
        code = opt.find('ID').text
        if code is None:
            print "Ignoring, ID is not set"
            continue

        title = opt.find('Title').text
        if title is None:
            print "Ignoring obj %s, Title is not set" % code
            continue

        group_code = opt.find('Group_ID').text
        if group_code is None:
            print "Ignoring obj %s, Group_ID not set" % code
            continue
        group_codes.append(group_code)

        group_title = opt.find('Group_Title').text
        if group_title is None:
            print "Ignoring obj %s, Group_Title not set" % code
            continue

        property_data = MultiDict([('code', group_code),
                                   ('title', group_title)])
        property_form = PropertyForm(property_data)

        property_obj = None
        if property_form.validate():
            property_obj = Property.get(code=group_code)
            if property_obj is None:
                prop = Property(title=property_form.title.data,
                                code=property_form.code.data)
                commit()
                property_obj = Property.get(id=prop.id)
                print "Property created"
            else:
                property_obj.title = property_form.title.data
                property_obj.code = property_form.code.data
                commit()
                print "Property %s updated" % property_obj.id
        else:
            print "Ignoring obj %s, errors: %s" % (code, property_form.errors)

        option_data = MultiDict([('code', code),
                                 ('title', title)])
        option_form = OptionForm(option_data)
        if option_form.validate():
            option = Option.get(code=code)
            if option is None:
                Option(title=option_form.title.data,
                       code=option_form.code.data,
                       property=property_obj)
                print "Option created"
            else:
                option.title = option_form.title.data
                option.code = option_form.code.data
                option.propert = property_obj
                commit()
                print "Option %s updated" % option.id
        else:
            print "Ignoring obj %s, errors: %s" % (code, option_form.errors)

    props_to_remove = select(p for p in Property if p.code is None or p.code not in group_codes)
    for p in props_to_remove:
        p.delete()


@manager.command
@db_session
def load():
    print 'Loading options'
    data = get_url_contents('http://d24.itliteclient.ru/1cexp/options.xml')
    load_options(xml.etree.ElementTree.fromstring(data).findall('option'))

    print 'Loading items'
    data = get_url_contents('http://d24.itliteclient.ru/1cexp/nomenclature.xml')
    load_items(xml.etree.ElementTree.fromstring(data).findall('NomenclatureGroup'))


if __name__ == "__main__":
    manager.run()
