import xml
from urllib2 import HTTPError

from pony.orm import db_session, commit

from model import Client, Balance
from server import facebook, app
from flask import url_for, request, session, redirect, render_template
from utils import get_url_contents


@facebook.tokengetter
def get_facebook_token():
    return session.get('facebook_token')


def pop_login_session():
    session.pop('logged_in', None)
    session.pop('facebook_token', None)
    session.pop('user_name', None)


@app.route("/login")
def facebook_login():
    return facebook.authorize(callback=url_for('facebook_authorized', next=request.args.get('next'), _external=True))


@app.route("/facebook_authorized")
@facebook.authorized_handler
@db_session
def facebook_authorized(resp):
    next_url = request.args.get('next') or url_for('index')
    if resp is None or 'access_token' not in resp:
        return redirect(next_url)

    session['logged_in'] = True
    session['facebook_token'] = (resp['access_token'], '')
    access_token = resp['access_token']

    if access_token is None or access_token == '':
        return redirect(url_for('index'))

    data = facebook.get('/me?locale=en_US&fields=name,email').data
    if 'id' in data and 'name' in data:
        session['user_name'] = data['name']

    if 'email' in data:
        email = data['email'].lower()
        client = Client.get(email=email)
        if client is None:
            client = Client(email=email,
                            name=data['name'],
                            facebook_id=data['id'],
                            facebook_token=access_token)
        else:
            client.facebook_token = access_token

        commit()

        if client:
            session['client_id'] = client.id

            try:
                data = get_url_contents('http://d24.itliteclient.ru/1cexp/rentcoins.xml')
                entries = xml.etree.ElementTree.fromstring(data).findall('RentcoinEntry')
                for obj in entries:
                    if obj.find('ClientEmail').text is not None and \
                            client.email == obj.find('ClientEmail').text.lower() and \
                            obj.find('RentcoinBalance').text and \
                            obj.find('RentcoinBalance').text.isdigit():
                        balance = Balance.get(client=client)
                        if balance is None:
                            Balance(balance=int(obj.find('RentcoinBalance').text), client=client)
                        else:
                            balance.balance=int(obj.find('RentcoinBalance').text)
            except HTTPError, e:
                pass

    return redirect(next_url)


@app.route("/logout")
def logout():
    pop_login_session()
    return redirect(url_for('index'))


@app.route("/profile")
@db_session
def profile():
    if 'logged_in' not in session or session['logged_in'] is not True:
        return redirect(url_for('index'))

    print session

    client = Client.get(id=session['client_id'])
    return render_template('profile.html', client=client)
