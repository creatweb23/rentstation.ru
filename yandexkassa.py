# coding=utf-8
from wtforms import Form, validators, HiddenField, RadioField


class YandexKassaForm(Form):
    # paymentType = RadioField(choices=[
    #     ('PC', u'Оплата из кошелька в Яндекс.Деньгах.'),
    #     ('AC', u'Оплата с произвольной банковской карты.'),
    #     ('MC', u'Платеж со счета мобильного телефона.'),
    #     ('GP', u'Оплата наличными через кассы и терминалы.'),
    #     ('WM', u'Оплата из кошелька в системе WebMoney.'),
    #     ('SB', u'Оплата через Сбербанк: оплата по SMS или Сбербанк Онлайн.'),
    #     ('MP', u'Оплата через мобильный терминал (mPOS).'),
    #     ('AB', u'Оплата через Альфа-Клик.'),
    #     ('AB', u'Оплата через MasterPass.'),
    #     ('PB', u'Оплата через Промсвязьбанк.')
    # ])
    shopId = HiddenField(validators=[validators.required])
    scid = HiddenField(validators=[validators.required])
    sum = HiddenField(validators=[validators.required])
    customerNumber = HiddenField(validators=[validators.required])
    orderNumber = HiddenField(validators=[validators.required])
    cps_phone = HiddenField(validators=[validators.required])
    cps_email = HiddenField(validators=[validators.required])
