# coding=utf-8
import random
from functools import wraps
import re
from flask import Flask, Response, _app_ctx_stack
from flask.ext.mail import Mail
from flask_oauth import OAuth
from jinja2 import evalcontextfilter, escape, Markup
from pony import orm
from flask.ext.httpauth import HTTPBasicAuth

app = Flask(__name__)
app.config.from_pyfile('default_config.py')
app.config.from_pyfile('config.py')
db = orm.Database('postgres', app.config['DATABASE'])
mail = Mail()
mail.init_app(app)
auth = HTTPBasicAuth()

from model import *

db.generate_mapping()

_paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')


def count_category_items(category):
    return BagManager.count(category=category)


app.jinja_env.globals.update(count_category_items=count_category_items)


@app.template_filter()
@evalcontextfilter
def nl2br(eval_ctx, value):
    result = u'\n\n'.join(u'%s<br/>' % p.replace('\n', '<br/>\n') for p in _paragraph_re.split(value))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result


rmonth = {
    1: u'янв.',
    2: u'февр.',
    3: u'марта',
    4: u'апр.',
    5: u'мая',
    6: u'июня',
    7: u'июля',
    8: u'авг.',
    9: u'сент.',
    10: u'окт.',
    11: u'нояб.',
    12: u'дек.',
}

rmonthi = {
    1: u'Январь',
    2: u'Февраль',
    3: u'Март',
    4: u'Апрель',
    5: u'Май',
    6: u'Июнь',
    7: u'Июль',
    8: u'Август',
    9: u'Сентябрь',
    10: u'Октябрь',
    11: u'Ноябрь',
    12: u'Декабрь',
}


@app.template_filter('datetime')
def _jinja2_filter_datetime(date):
    return u"%s %s %s %s" % (date.day, rmonth[date.month], date.year, date.strftime('%H:%M'))


@app.template_filter('date')
def _jinja2_filter_datetime(date):
    return u"%s %s %s" % (date.day, rmonth[date.month], date.year)


@app.template_filter('month')
def _jinja2_filter_month(date):
    return u"%s %s" % (rmonthi[date.month], date.year)


@app.template_filter()
def number_format(value, tsep=',', dsep='.'):
    s = unicode(value)
    cnt = 0
    numchars = dsep + '0123456789'
    ls = len(s)
    while cnt < ls and s[cnt] not in numchars:
        cnt += 1

    lhs = s[:cnt]
    s = s[cnt:]
    if not dsep:
        cnt = -1
    else:
        cnt = s.rfind(dsep)
    if cnt > 0:
        rhs = dsep + s[cnt+1:]
        s = s[:cnt]
    else:
        rhs = ''

    splt = ''
    while s != '':
        splt = s[-3:] + tsep + splt
        s = s[:-3]

    return lhs + splt[:-1] + rhs


@app.template_filter('shuffle')
def filter_shuffle(seq, slice=None):
    try:
        result = list(seq)
        random.shuffle(result)
        if slice is not None:
            result = result[:slice]
        return result
    except:
        return seq


@app.context_processor
def inject_user():
    return dict(current_username=auth.username())


@app.before_first_request
def init_db():
    db.disconnect()


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'db'):
        top.db.close()


@auth.get_password
def get_pw(username):
    users = app.config['USERS']
    if username in users:
        return users.get(username)
    return None


def required_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if auth.username() != 'admin':
            return auth.auth_error_callback()
        return f(*args, **kwargs)

    return decorated


oauth = OAuth()

facebook = oauth.remote_app(
    'facebook',
    base_url='https://graph.facebook.com/',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    consumer_key=app.config['FACEBOOK_APP_ID'],
    consumer_secret=app.config['FACEBOOK_APP_SECRET'],
    request_token_params={'scope': ('email, ')}
)

from admin import *
from client import *
from kassa import *
from integraion import *
from fb import *

