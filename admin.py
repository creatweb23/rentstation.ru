# coding=utf-8
import os
import uuid
from PIL import Image
import datetime
from flask import render_template, request, url_for, flash, Response, make_response
from pony.orm import db_session, commit
from werkzeug.exceptions import NotFound
from werkzeug.utils import secure_filename, redirect
from model import BagManager, BagForm, Bag, BookingAdminForm, Booking, InvoiceManager, Invoice, InvoiceAdminForm, \
    check_consistency, check_min_period, CategoryManager, BookingManager, AddressManager, ActTemplateManager, \
    PropertyManager, PropertyForm, Property, OptionForm, Option, OptionManager, Address, AddressForm
from server import app, auth, required_admin
from dateutil.parser import parse
from dateutil import relativedelta
from pdfview import invoice_pdf_view


@app.route('/admin/')
@app.route('/admin')
@auth.login_required
def dashboard():
    if auth.username() == 'admin':
        return redirect(url_for('admin_invoice_list'))
    else:
        if auth.username() == 'veloprobeg':
            return redirect(url_for('admin_address_booking_list', address_id=1))
        if auth.username() == 'trisport':
            return redirect(url_for('admin_address_booking_list', address_id=2))
        if auth.username() == 'triatleta':
            return redirect(url_for('admin_address_booking_list', address_id=3))
        if auth.username() == 'krylatskoe':
            return redirect(url_for('admin_address_booking_list', address_id=4))
        if auth.username() == 'snowbelka':
            return redirect(url_for('admin_address_booking_list', address_id=9))


@app.route('/admin/bag/list')
@auth.login_required
@required_admin
@db_session
def admin_bag_list():
    items = BagManager.find(show_hidden=True)
    return render_template('admin/bag_list.html', bags=items)


# @app.route('/admin/address/list')
# @auth.login_required
# @required_admin
# @db_session
# def admin_address_list():
#     items = AddressManager.find()
#     return render_template('admin/address_list.html', items=items)
#
#
def make_thumb(file_data):
    if isinstance(file_data, (bytes, str)):
        im = Image.open(app.config['UPLOAD_FOLDER'] + file_data)
    else:
        im = Image.open(file_data)
    im.thumbnail(app.config['UPLOAD_SIZE'], Image.ANTIALIAS)
    w, h = im.size
    w0, h0 = app.config['UPLOAD_SIZE']
    new = Image.new('RGBA', app.config['UPLOAD_SIZE'], (255, 255, 255, 255))
    new.paste(im, ((w0-w)/2, (h0-h)/2))

    outfile = "thumb_%s.jpg" % secure_filename(str(uuid.uuid1().int))
    new.save(app.config['UPLOAD_FOLDER'] + outfile, "PNG")
    return outfile


def upload_image(file):
    filename = secure_filename(str(uuid.uuid1().int) + file.filename)
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return make_thumb(filename)


@app.route('/admin/bag/create', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_bag_create():
    form = BagForm(request.form)
    form.category.choices = [(str(i.id), i.title) for i in CategoryManager.find()]
    form.option.choices = [(str(i.id), "%s %s" % (i.property.title, i.title)) for i in OptionManager.find()]
    form.address_id.choices = [(str(i.id), i.title) for i in AddressManager.find()]
    form.act_template_id.choices = [(str(i.id), i.title) for i in ActTemplateManager.find()]
    if request.form and request.files['image'] and form.validate():
        categories = CategoryManager.find(item_id=map(int, form.category.data))
        options = OptionManager.find(ids=map(int, form.option.data))
        address = AddressManager.get(item_id=map(int, [form.address_id.data]))
        act_template = ActTemplateManager.get(item_id=map(int, form.act_template_id.data))
        image_name = upload_image(request.files['image'])
        Bag(title=form.title.data,
            category=categories,
            option=options,
            price=form.price.data,
            deposit=form.deposit.data,
            estimate=form.estimate.data,
            image=image_name,
            description_short=form.description_short.data,
            description=form.description.data,
            address=address.id,
            act_template=act_template.id,
            comments=form.comments.data,
            is_discount=form.is_discount.data,
            is_disabled=form.is_disabled.data,
            min_period=form.min_period.data,
            is_to_rent=True)
        return redirect(url_for('admin_bag_list'))
    else:
        return render_template('admin/bag_form.html', form=form, is_create=True, bag=None)


# @app.route('/admin/bag/<int:bag_id>/edit', methods=['GET', 'POST'])
# @auth.login_required
# @required_admin
# @db_session
# def admin_bag_edit(bag_id):
#     bag = Bag.get(id=bag_id)
#     form = BagForm(request.form, bag, coerce=str)
#     form.category.choices = [(str(i.id), i.title) for i in CategoryManager.find()]
#     form.option.choices = [(str(i.id), "%s %s" % (i.property.title, i.title)) for i in OptionManager.find()]
#     form.address_id.choices = [(str(i.id), i.title) for i in AddressManager.find()]
#     form.act_template_id.choices = [(str(i.id), i.title) for i in ActTemplateManager.find()]
#     return render_template('admin/bag_form.html', form=form, bag=bag)


@app.route('/admin/bag/<int:bag_id>/edit', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_bag_edit(bag_id):
    bag = Bag.get(id=bag_id)
    form = BagForm(request.form, bag, coerce=str)
    form.category.choices = [(str(i.id), i.title) for i in CategoryManager.find()]
    form.option.choices = [(str(i.id), "%s %s" % (i.property.title, i.title)) for i in OptionManager.find()]
    form.address_id.choices = [(str(i.id), i.title) for i in AddressManager.find()]
    form.act_template_id.choices = [(str(i.id), i.title) for i in ActTemplateManager.find()]
    if request.form and form.validate():
        categories = CategoryManager.find(item_id=map(int, form.category.data))
        options = OptionManager.find(ids=map(int, form.option.data))
        address = AddressManager.get(item_id=map(int, [form.address_id.data]))
        act_template = ActTemplateManager.get(item_id=map(int, form.act_template_id.data))
        bag.title = form.title.data
        bag.category = categories
        bag.option = options
        bag.address = address.id
        bag.act_template = act_template.id
        bag.price = form.price.data
        bag.deposit = form.deposit.data
        bag.estimate = form.estimate.data
        bag.description_short = form.description_short.data
        bag.description = form.description.data
        bag.comments = form.comments.data
        bag.is_discount = form.is_discount.data
        bag.is_disabled = form.is_disabled.data
        bag.min_period = form.min_period.data
        if request.files['image']:
            image_name = upload_image(request.files['image'])
            bag.image = image_name
        return redirect(url_for('admin_bag_list'))
    else:
        return render_template('admin/bag_form.html', form=form, bag=bag)


@app.route('/admin/bag/<int:bag_id>/delete')
@auth.login_required
@required_admin
@db_session
def admin_bag_delete(bag_id):
    bag = Bag.get(id=bag_id)
    if len(bag.booking) is 0:
        bag.delete()
    else:
        flash(u'Невозможно удалить %s, есть заказы с этим товаром: %s' % (
            bag.title, ', '.join(map(str, [b.invoice.id for b in bag.booking]))))
    return redirect(url_for('admin_bag_list'))


@app.route('/admin/invoice/<int:invoice_id>/booking/create', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_booking_create(invoice_id):
    form = BookingAdminForm(request.form)
    form.invoice_id.data = invoice_id
    form.item_id.choices = [(str(i.id), i.title) for i in BagManager.find()]
    form.validate()
    if request.form and form.validate():
        start = parse(form.start.data)
        end = parse(form.end.data)
        item = Bag.get(id=form.item_id.data)
        if start > end:
            start, end = end, start
        if not check_consistency(item, start, end):
            form.start.errors.append(u'В указанный период товар недоступен')
        elif not check_min_period(min_period=item.min_period, start=start, end=end):
            form.start.errors.append(
                u'Минимальный период аренды товара - %s' % (u'одна неделя' if item.min_period == 1 else u'один месяц'))
        else:
            invoice = Invoice.get(id=form.invoice_id.data)
            booking = Booking(item=item,
                              invoice=invoice,
                              start=start,
                              end=end)
            booking.confirm_price = booking.price
            return redirect(url_for('admin_invoice_edit', invoice_id=form.invoice_id.data))
    return render_template('admin/booking_form.html', form=form, is_create=True, booking=None)


@app.route('/admin/booking/<int:booking_id>/edit', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_booking_edit(booking_id):
    booking = Booking.get(id=booking_id)
    form = BookingAdminForm(request.form, booking, coerce=str)
    form.item_id.choices = [(str(i.id), u"Арт.%s %s" % (i.art, i.title)) for i in BagManager.find(show_hidden=True)]
    if request.form and form.validate():
        start = parse(form.start.data)
        form.start.data = start
        end = parse(form.end.data)
        form.end.data = end
        item = Bag.get(id=form.item_id.data)
        if start > end:
            start, end = end, start
        if not check_consistency(item, start, end, ignore_booking=booking):
            form.start.errors.append(u'В указанный период товар недоступен')
        elif not check_min_period(min_period=item.min_period, start=start, end=end):
            form.start.errors.append(
                u'Минимальный период аренды товара - %s' % (u'одна неделя' if item.min_period == 1 else u'один месяц'))
        else:
            booking.item = Bag.get(id=int(form.item_id.data))
            booking.start = start
            booking.end = end
            booking.confirm_price = booking.price
            return redirect(url_for('admin_invoice_edit', invoice_id=booking.invoice.id))
    return render_template('admin/booking_form.html', form=form, booking=booking)


@app.route('/admin/booking/<int:booking_id>/delete')
@auth.login_required
@required_admin
@db_session
def admin_booking_delete(booking_id):
    booking = Booking.get(id=booking_id)
    invoice_id = booking.invoice.id
    booking.delete()
    return redirect(url_for('admin_invoice_edit', invoice_id=invoice_id))


@app.route('/admin/invoice/<int:invoice_id>/delete')
@auth.login_required
@required_admin
@db_session
def admin_invoice_delete(invoice_id):
    invoice = Invoice.get(id=invoice_id)
    if invoice.is_payed is False:
        invoice.delete()
    else:
        flash(u'Невозможно удалить заказ #%s, т.к. заказ оплачен' % invoice.id)
    return redirect(url_for('admin_invoice_list'))


@app.route('/admin/invoice/list')
@auth.login_required
@required_admin
@db_session
def admin_invoice_list():
    invoice_list = InvoiceManager.find(text=request.args.get('text'))
    return render_template('admin/invoice_list.html', items=invoice_list)


@app.route('/admin/address/<int:address_id>/booking/list')
@auth.login_required
@db_session
def admin_address_booking_list(address_id):
    if auth.username() == 'veloprobeg' and address_id != 1:
        return auth.auth_error_callback()
    if auth.username() == 'trisport' and address_id != 2:
        return auth.auth_error_callback()
    if auth.username() == 'triatleta' and address_id != 3:
        return auth.auth_error_callback()
    if auth.username() == 'krylatskoe' and address_id != 4:
        return auth.auth_error_callback()
    if auth.username() == 'snowbelka' and address_id != 9:
        return auth.auth_error_callback()
    booking_list = BookingManager.find(address_id)
    return render_template('admin/address_booking_list.html', items=booking_list)


@app.route('/admin/booking/<int:booking_id>/give')
@auth.login_required
@db_session
def admin_booking_give(booking_id):
    booking = Booking.get(id=booking_id)
    if booking.give_at is None:
        booking.give_at = datetime.datetime.now()
        booking.updated_at = datetime.datetime.now()
    return "OK"


@app.route('/admin/booking/<int:booking_id>/return')
@auth.login_required
@db_session
def admin_booking_return(booking_id):
    booking = Booking.get(id=booking_id)
    if booking.return_at is None:
        booking.return_at = datetime.datetime.now()
        booking.updated_at = datetime.datetime.now()
    return "OK"


@app.route('/admin/booking/list.csv')
@auth.login_required
@required_admin
@db_session
def admin_booking_list_csv():
    booking_list = BookingManager.find()
    response = make_response(render_template('admin/booking_list.csv', booking_list=booking_list))
    response.headers["Content-Disposition"] = "attachment; filename=bookings.csv"
    return response


@app.route('/admin/invoice/<int:invoice_id>/edit', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_invoice_edit(invoice_id):
    invoice = Invoice.get(id=invoice_id)
    print request.form
    form = InvoiceAdminForm(request.form, invoice)
    if request.form and form.validate():
        invoice.person_lastname = form.person_lastname.data
        invoice.person_firstname = form.person_firstname.data
        invoice.person_phone = form.person_phone.data
        invoice.person_email = form.person_email.data
        invoice.is_owner = form.is_owner.data
        invoice.is_payed = form.is_payed.data
        invoice.is_trilife = form.is_trilife.data
        invoice.is_deposit = form.is_deposit.data
        invoice.comments = form.comments.data
        return redirect(url_for('admin_invoice_list'))
    return render_template('admin/invoice_form.html', item=invoice, form=form)


@app.route('/admin/invoice/create', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_invoice_create():
    form = InvoiceAdminForm(request.form)
    if request.form and form.validate():
        invoice = Invoice(person_lastname=form.person_lastname.data,
                          person_firstname=form.person_firstname.data,
                          person_phone=form.person_phone.data,
                          person_email=form.person_email.data,
                          is_owner=form.is_owner.data,
                          is_payed=form.is_payed.data,
                          is_trilife=form.is_trilife.data,
                          comments=form.comments.data)
        commit()
        return redirect(url_for('admin_invoice_edit', invoice_id=invoice.id))
    return render_template('admin/invoice_form.html', form=form, is_create=True)


@app.route('/admin/booking/<int:booking_id>/act', methods=['GET'])
@auth.login_required
@db_session
def admin_booking_act(booking_id):
    booking = Booking.get(id=booking_id)
    if not booking:
        raise NotFound()
    return Response(invoice_pdf_view(booking_number=booking.number,
                                     person=u'%s %s' % (
                                         booking.invoice.person_firstname, booking.invoice.person_lastname),
                                     phone=booking.invoice.person_phone,
                                     email=booking.invoice.person_email,
                                     title=booking.item.title,
                                     item_id=booking.item.art,
                                     estimate=booking.item.estimate,
                                     deposit=booking.item.deposit,
                                     start=booking.start.strftime("%d/%m/%Y"),
                                     end=booking.end.strftime("%d/%m/%Y"),
                                     period=booking.days,
                                     price=booking.confirm_price or booking.price,
                                     template=booking.item.act_template.template,
                                     is_deposit=booking.invoice.is_deposit),
                    mimetype='application/pdf')


def diff_month(d1, d2):
    return (d1.year - d2.year) * 12 + d1.month - d2.month


@app.route('/admin/stat', methods=['GET'])
@auth.login_required
@required_admin
@db_session
def admin_stat():
    items = []
    start = datetime.datetime(2014, 12, 1)
    for delta in range(0, diff_month(datetime.datetime.today(), start) + 1):
        period = start + relativedelta.relativedelta(months=delta)
        end = period + relativedelta.relativedelta(months=1)
        items.append({'period': period,
                      'count_invoice': InvoiceManager.count_by_period(period, end),
                      'sum_invoice': InvoiceManager.sum_by_period(period, end)})
    return render_template('admin/stat.html', items=items)


@app.route('/admin/meet', methods=['GET'])
@auth.login_required
@required_admin
@db_session
def admin_meet():
    items = []
    start = (datetime.datetime.today() - relativedelta.relativedelta(days=14)).date()
    delta = (datetime.datetime.today() + relativedelta.relativedelta(days=7)).date() - start
    for delta in range(0, delta.days + 1):
        period = start + relativedelta.relativedelta(days=delta)
        items.append({'period': period,
                      'give': BookingManager.find_by_start(period),
                      'return': BookingManager.find_by_end(period)})
    return render_template('admin/meet.html', items=items, today=datetime.datetime.today())


@app.route('/admin/property/list')
@auth.login_required
@required_admin
@db_session
def admin_property_list():
    items = PropertyManager.find()
    return render_template('admin/property_list.html', items=items)


@app.route('/admin/property/create', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_property_create():
    form = PropertyForm(request.form)
    if request.form and form.validate():
        Property(title=form.title.data)
        return redirect(url_for('admin_property_list'))
    else:
        return render_template('admin/property_form.html', form=form, is_create=True, item=None)


@app.route('/admin/property/<int:property_id>/edit', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_property_edit(property_id):
    property = Property.get(id=property_id)
    form = PropertyForm(request.form, property, coerce=str)
    if request.form and form.validate():
        property.title = form.title.data
        return redirect(url_for('admin_property_list'))
    return render_template('admin/property_form.html', form=form, item=property)


@app.route('/admin/property/<int:property_id>/delete')
@auth.login_required
@required_admin
@db_session
def admin_property_delete(property_id):
    property = Property.get(id=property_id)
    if len(property.option) is 0:
        property.delete()
    else:
        flash(u'Невозможно удалить %s, есть опции с этой характеристикой: %s' % (
            property.title, ', '.join(property.option)))
    return redirect(url_for('admin_property_list'))


@app.route('/admin/property/<int:property_id>/option/create', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_option_create(property_id):
    form = OptionForm(request.form)
    if request.form and form.validate():
        Option(title=form.title.data, property=Property.get(id=property_id))
        return redirect(url_for('admin_property_edit', property_id=property_id))
    else:
        return render_template('admin/option_form.html', form=form, property_id=property_id, is_create=True, item=None)


@app.route('/admin/property/<int:property_id>/option/<int:option_id>/edit', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_option_edit(property_id, option_id):
    option = Option.get(id=option_id)
    form = OptionForm(request.form, option, coerce=str)
    if request.form and form.validate():
        option.title = form.title.data
        return redirect(url_for('admin_property_edit', property_id=property_id))
    return render_template('admin/option_form.html', form=form, item=option, property_id=property_id)


@app.route('/admin/property/<int:property_id>/option/<int:option_id>/delete')
@auth.login_required
@required_admin
@db_session
def admin_option_delete(property_id, option_id):
    option = Option.get(id=option_id)
    if len(option.item) is 0:
        option.delete()
    else:
        flash(u'Невозможно удалить %s, есть товары с этой опцией: %s' % (
            option.title, ', '.join(option.item)))
    return redirect(url_for('admin_property_edit', property_id=property_id))


@app.route('/admin/address/list')
@auth.login_required
@required_admin
@db_session
def admin_address_list():
    items = AddressManager.find()
    return render_template('admin/address_list.html', items=items)


@app.route('/admin/address/create', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_address_create():
    form = AddressForm(request.form)
    if request.form and form.validate():
        Address(title=form.title.data,
                city_title=form.city_title.data,
                address=form.address.data,
                city_id=form.city_id.data
                )
        return redirect(url_for('admin_address_list'))
    else:
        return render_template('admin/address_form.html', form=form, is_create=True, item=None)


@app.route('/admin/address/<int:address_id>/edit', methods=['GET', 'POST'])
@auth.login_required
@required_admin
@db_session
def admin_address_edit(address_id):
    address = Address.get(id=address_id)
    form = AddressForm(request.form, address, coerce=str)
    if request.form and form.validate():
        address.title = form.title.data
        address.city_title = form.city_title.data
        address.address = form.address.data
        address.city_id = form.city_id.data
        return redirect(url_for('admin_address_list'))
    return render_template('admin/address_form.html', form=form, item=address)


@app.route('/admin/address/<int:address_id>/delete')
@auth.login_required
@required_admin
@db_session
def admin_address_delete(address_id):
    address = Address.get(id=address_id)
    if len(address.item) is 0:
        address.delete()
    else:
        flash(u'Невозможно удалить %s, есть товары с этой точкой выдачи: %s' % (
            address.title, ', '.join(address.item)))
    return redirect(url_for('admin_address_edit', address_id=address_id))
