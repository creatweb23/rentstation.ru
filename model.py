# coding=utf-8
from collections import defaultdict
from datetime import datetime, date
import hashlib
from dateutil.relativedelta import relativedelta
from flask import url_for
from pony import orm
from pony.orm import count
from wtforms import Form, StringField, SelectField, BooleanField, IntegerField, validators, SelectMultipleField
from wtforms.validators import Email
from wtforms.widgets import TextArea, CheckboxInput, HTMLString
from server import db, app
from base64 import urlsafe_b64encode
from uuid import uuid4 as uuid


class Address(db.Entity):
    _table_ = 'address'
    title = orm.Required(unicode, nullable=False)
    address = orm.Optional(unicode)
    items = orm.Set('Bag')
    code = orm.Optional(str)
    city_title = orm.Optional(unicode)
    ya_sid = orm.Optional(str)
    city_id = orm.Optional(int)
    url = orm.Optional(str)


class AddressManager(object):
    @staticmethod
    def find(city_id=None):
        items = orm.select(item for item in Address).order_by(orm.desc(Address.id))
        if city_id is not None:
            items = filter(lambda x: x.city_id == city_id, items)
        return items

    @staticmethod
    def get(item_id):
        return orm.get(item for item in Address if item.id in item_id)


class ActTemplate(db.Entity):
    _table_ = 'act_template'
    title = orm.Required(unicode, nullable=False)
    template = orm.Optional(unicode)
    items = orm.Set('Bag')
    code = orm.Optional(str)


class ActTemplateManager(object):
    @staticmethod
    def get(item_id):
        return orm.get(item for item in ActTemplate if item.id in item_id)

    @staticmethod
    def find():
        return orm.select(item for item in ActTemplate).order_by(orm.desc(ActTemplate.id))


class Bag(db.Entity):
    _table_ = 'bag'
    title = orm.Required(unicode, nullable=False)
    image = orm.Required(str, nullable=False)
    category = orm.Set('Category', table='bag_category', column='category_id')
    description_short = orm.Optional(unicode)
    description = orm.Optional(unicode)
    price = orm.Required(int, nullable=False, default=200)
    sell_price = orm.Required(int, nullable=False, default=0)
    deposit = orm.Required(int, nullable=False, default=0)
    estimate = orm.Required(int, nullable=False, default=0)
    booking = orm.Set('Booking')
    is_discount = orm.Optional(bool)
    is_disabled = orm.Optional(bool)
    min_period = orm.Optional(int)
    comments = orm.Optional(unicode)
    address = orm.Required(Address)
    act_template = orm.Required(ActTemplate)
    created_at = orm.Required(datetime, default=datetime.now)
    updated_at = orm.Required(datetime, default=datetime.now)
    option = orm.Set('Option', table='bag_option', column='option_id')
    code = orm.Optional(str)
    image_code = orm.Optional(str)
    # art = orm.Required(int)
    is_sold = orm.Optional(bool)
    is_to_rent = orm.Optional(bool)
    is_to_sell = orm.Optional(bool)
    slug = orm.Optional(str)

    @property
    def art(self):
        return self.id

    @property
    def address_id(self):
        return self.address.id

    @property
    def act_template_id(self):
        return self.act_template.id

    @property
    def image_path(self):
        return app.config['UPLOAD_PATH'] + self.image

    @property
    def actual_booking(self):
        return Booking.select(lambda x: x.item == self and x.end >= date.today()).order_by(Booking.start)

    def to_dict(self):
        return {'id': self.id,
                'booking': self.actual_booking}

    @property
    def url(self):
        return url_for('item_slug_resource', slug=self.slug) if self.slug else url_for('item_resource', item_id=self.id)

    def __unicode__(self):
        return "#%s %s" % (self.id, self.title)


def check_consistency(bag, start, end, ignore_booking=None):
    if ignore_booking is None:
        booking_count = orm.select(b for b in Booking if
                                   b.item == bag and
                                   b.start <= end and start <= b.end)
    else:
        booking_count = orm.select(b for b in Booking if
                                   b != ignore_booking and
                                   b.item == bag and
                                   b.start <= end and start <= b.end)
    if count(booking_count) != 0:
        return False
    else:
        return True


def check_min_period(min_period, start, end):
    if min_period == 1:
        min_end = start + relativedelta(weeks=1) - relativedelta(days=1)
    elif min_period == 2:
        min_end = start + relativedelta(months=1) - relativedelta(days=1)
    else:
        min_end = start
    return end >= min_end


def days(start, end):
    delta = end - start
    return delta.days + 1


def md5(s):
    return hashlib.md5(s).hexdigest()


class Category(db.Entity):
    _table_ = 'category'
    bag = orm.Set(Bag, column="item_id")
    title = orm.Required(unicode, nullable=False)
    weight = orm.Optional(int)
    code = orm.Optional(str)
    slug = orm.Optional(str)
    page_title = orm.Optional(unicode)

    @property
    def url(self):
        return url_for('catalog_slug_category', slug=self.slug) if self.slug \
            else url_for('catalog_category', category_id=self.id)

    def __unicode__(self):
        return self.title


class CategoryManager(object):
    @staticmethod
    def find(item_id=None):
        if item_id is None:
            return orm.select(item for item in Category).order_by(orm.desc(Category.weight))
        else:
            return orm.select(item for item in Category if item.id in item_id).order_by(orm.desc(Category.weight))


class Invoice(db.Entity):
    _table_ = 'invoice'
    booking = orm.Set('Booking')
    person_lastname = orm.Optional(unicode)
    person_firstname = orm.Optional(unicode)
    person_email = orm.Optional(unicode)
    person_phone = orm.Optional(unicode)
    is_trilife = orm.Optional(bool)
    is_deposit = orm.Optional(bool)
    is_owner = orm.Optional(bool)
    is_payed = orm.Optional(bool)
    comments = orm.Optional(unicode)
    shipping_price = orm.Optional(int, default=0)
    uid = orm.Optional(str)
    created_at = orm.Required(datetime, default=datetime.now)
    updated_at = orm.Required(datetime, default=datetime.now)

    def __init__(self):
        super(Invoice, self).__init__()
        self.uid = urlsafe_b64encode(uuid().bytes)[0:22]

    @property
    def address_list(self):
        address_list = defaultdict(list)
        for booking in self.booking:
            address_list[booking.item.address.address].append(booking.item.title)
        return address_list

    @property
    def url(self):
        return url_for('invoice', uid=self.uid)

    @property
    def price(self):
        return sum([b.confirm_price or b.price for b in self.booking]) + int(self.shipping_price or 0)

    @property
    def description(self):
        return u'Заказ #%s' % self.id + ', '.join(u"%s (артикул %s) с %s по %s" % (
            b.item.title,
            b.item.art,
            b.start.strftime('%Y-%m-%d'),
            b.end.strftime('%Y-%m-%d')
        ) for b in self.booking) + u' ,скидка 50%' if self.is_trilife else u' ,без скидки'

    def to_dict(self):
        return {'id': self.id,
                'booking': self.booking,
                'is_trilife': self.is_trilife,
                'person_lastname': self.person_lastname,
                'person_firstname': self.person_firstname,
                'person_email': self.person_email,
                'person_phone': self.person_phone,
                'shipping_price': self.shipping_price}

    def get_signature_request(self, login, password1):
        return md5(":".join(map(str, [login, self.price, self.id, password1])))

    def get_signature_responce(self, password, out_sum=None):
        out_sum = str(float(self.price)) if out_sum is None else out_sum
        return md5(':'.join(map(str, [out_sum, self.id, password])))


class Booking(db.Entity):
    _table_ = 'booking'
    item = orm.Required('Bag', column='item_id', nullable=False)
    start = orm.Required(datetime, nullable=False)
    end = orm.Required(datetime, nullable=False)
    invoice = orm.Required('Invoice', column='invoice_id', nullable=False)
    confirm_price = orm.Optional(int, default=0)
    created_at = orm.Required(datetime, default=datetime.now)
    updated_at = orm.Required(datetime, default=datetime.now)
    give_at = orm.Optional(datetime, nullable=True)
    return_at = orm.Optional(datetime, nullable=True)

    @property
    def number(self):
        n = 0
        for b in self.invoice.booking:
            n += 1
            if b.id == self.id:
                break
        return "%s/%s" % (self.invoice.id, n)

    @property
    def item_id(self):
        return self.item.art

    @property
    def invoice_id(self):
        return self.invoice.id

    @property
    def price_full(self):
        return self.days * self.item.price

    @property
    def price(self):
        if self.invoice.is_trilife and self.item.is_discount:
            return self.discount_price
        else:
            return self.price_full

    @property
    def discount(self):
        return self.price_full - self.price

    @property
    def discount_price(self):
        return int(self.days * self.item.price / 2)

    def to_dict(self):
        return {'id': self.id,
                'start': self.start,
                'end': self.end}

    def before_insert(self):
        self.proper_order()
        self.check_consistency()
        self.check_min_period()

    def before_update(self):
        self.before_insert()

    def proper_order(self):
        if self.start > self.end:
            self.start, self.end = self.end, self.start

    def check_consistency(self):
        booking_count = orm.select(b for b in Booking if
                                   b.id != self.id and b.item == self.item and
                                   b.start <= self.end and self.start <= b.end)
        if count(booking_count) != 0:
            raise ValueError('conflicts with booking %s' % booking_count.first().id)

    def check_min_period(self):
        if not check_min_period(min_period=self.item.min_period,
                                start=self.start,
                                end=self.end):
            raise ValueError('conflicts min_period')

    @property
    def days(self):
        return days(start=self.start, end=self.end)


class Property(db.Entity):
    title = orm.Required(unicode)
    option = orm.Set('Option')
    code = orm.Optional(str)


class Option(db.Entity):
    title = orm.Required(unicode)
    property = orm.Required('Property', column='property_id', nullable=False)
    item = orm.Set('Bag', column='item_id')
    code = orm.Optional(str)

    def __unicode__(self):
        return self.title


class Client(db.Entity):
    email = orm.Required(str)
    name = orm.Required(unicode)
    facebook_token = orm.Required(str)
    facebook_id = orm.Required(str)
    balance = orm.Optional('Balance')
    created_at = orm.Required(datetime, default=datetime.now)


class Balance(db.Entity):
    client = orm.Required('Client', column='client_id', nullable=False)
    balance = orm.Required(int, default=0)


class OptionManager(object):
    @staticmethod
    def find(ids=None):
        items = orm.select(item for item in Option).order_by(orm.desc(Option.id))
        if ids:
            items = items.filter(lambda x: x.id in ids)
        return items


class PropertyManager(object):
    @staticmethod
    def find():
        return orm.select(item for item in Property).order_by(orm.desc(Property.id))

    @staticmethod
    def find_for_category(category):
        items = BagManager.find(category=category)
        opt_ids = []
        for i in items:
            opt_ids.extend([o.id for o in i.option])
        opt_ids = list(set(opt_ids))
        props = orm.select(p for p in Property).order_by(orm.desc(Property.id))
        res_props = []
        for p in props:
            to_show = False
            for o in p.option:
                if o.id in opt_ids:
                    to_show = True
            if to_show:
                res_props.append(p)
        return res_props


class BagManager(object):
    @staticmethod
    def top10(city_id=None, address_id=None):
        items = orm.select(
            (b.item, count(b)) for b in Booking if b.item.is_disabled is False and b.item.is_to_rent is True)\
            .order_by(lambda: orm.desc(count(b)))
        booked = orm.select((b.item, count(b)) for b in Booking if b.end >= date.today())
        denyed = [i for i, cnt in booked if cnt > 2]
        if denyed:
            items = items.filter(lambda x, ccnt: x not in denyed)
        if city_id:
            items = items.filter(lambda x, ccnt: x.address.city_id == city_id)
        if address_id:
            items = items.filter(lambda x, ccnt: x.address.id == address_id)
        return items

    @staticmethod
    def find_new(city_id=None):
        items = orm.select(item for item in Bag if item.is_disabled is False and item.is_to_rent is True).order_by(orm.desc(Bag.created_at))
        if city_id:
            items = items.filter(lambda x: x.address.city_id == city_id)
        items = items.limit(12)
        return items

    @staticmethod
    def shop(city_id=None):
        items = orm.select(item for item in Bag if item.is_disabled is False).order_by(orm.desc(Bag.id))
        items = items.filter(lambda item: item.is_to_sell is True)
        if city_id:
            items = items.filter(lambda x: x.address.city_id == city_id)
        return items

    @staticmethod
    def find(category=None, options=None, show_hidden=None, city_id=None, is_sold=None):
        items = orm.select(item for item in Bag).order_by(orm.desc(Bag.id))
        items = items.filter(lambda item: item.is_to_rent is True)

        if category is not None:
            items = items.filter(lambda x: category in x.category)

        if city_id:
            items = items.filter(lambda x: x.address is not None and x.address.city_id == city_id)

        # if not show_hidden:
        #     for item in items:
        #         print item.id, item.is_disabled
        #     items = items.filter(lambda x: x.is_disabled is False)

        if options:
            items_by_prop = defaultdict(list)
            for o in options:
                items_by_prop[o.property.id].extend(o.item)
            f_items = set.intersection(*map(set, items_by_prop.values()))
            items = items.filter(lambda x: x in f_items)

        if is_sold is not None:
            items = items.filter(lambda x: x.is_sold is is_sold)

        return items

    @staticmethod
    def count(category=None):
        if category is None:
            return orm.count(item for item in Bag if item.is_disabled is False and item.item.is_to_rent is True)
        else:
            return orm.count(item for item in Bag if item.is_disabled is False and category in item.category and item.item.is_to_rent is True)

    @staticmethod
    def not_overlaps(start, end):
        return Bag.select_by_sql('select b.id, b.title, b.image from Bag b where b.id not in '
                                 '(select bo.bag_id from booking bo where bo.start <= $end and $start <= bo.end)',
                                 dict(start=start, end=end))


class BookingManager(object):
    @staticmethod
    def find(address_id=None):
        if address_id:
            return orm.select(b for b in Booking if b.item.address.id == address_id).order_by(orm.desc(Booking.id))
        else:
            return orm.select(b for b in Booking).order_by(orm.desc(Booking.id))

    @staticmethod
    def find_by_start(period):
        return orm.select(b for b in Booking if b.start == period and b.invoice.is_payed is True).order_by(
            orm.desc(Booking.id))

    @staticmethod
    def find_by_end(period):
        return orm.select(b for b in Booking if b.end == period and b.invoice.is_payed is True).order_by(
            orm.desc(Booking.id))


class InvoiceManager(object):
    @staticmethod
    def count_by_period(start, end):
        return orm.count(
            i for i in Invoice if i.created_at >= start and i.created_at < end and not i.is_owner and i.is_payed)

    @staticmethod
    def sum_by_period(start, end):
        items = orm.select(
            i for i in Invoice if i.created_at >= start and i.created_at < end and not i.is_owner and i.is_payed)
        return sum(i.price for i in items)

    @staticmethod
    def find(text=None):
        if text is None:
            return orm.select(i for i in Invoice).order_by(orm.desc(Invoice.id))
        else:
            return orm.select(i for i in Invoice if
                              text.lower() in i.person_firstname.lower() or text.lower() in i.person_lastname.lower() or text.lower() in i.person_email.lower() or text.lower() in i.person_phone.lower()).order_by(
                orm.desc(Invoice.id))


class PlainListWidget(object):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        html = []
        for subfield in field:
            html.append('<div class="checkbox"><label>%s %s</label></div>' % (subfield(), subfield.label))
        return HTMLString(''.join(html))


class MultiCheckboxField(SelectMultipleField):
    widget = PlainListWidget()
    option_widget = CheckboxInput()


class BagForm(Form):
    title = StringField()
    category = MultiCheckboxField(choices=[])
    option = MultiCheckboxField(choices=[])
    price = IntegerField()
    sell_price = IntegerField()
    deposit = IntegerField()
    estimate = IntegerField()
    image = StringField()
    description_short = StringField(widget=TextArea())
    description = StringField(widget=TextArea())
    is_discount = BooleanField()
    is_disabled = BooleanField()
    min_period = SelectField(choices=[('0', u'Без ограничений'), ('1', u'Неделя'), ('2', u'Месяц')])
    comments = StringField(widget=TextArea())
    address_id = SelectField(choices=[])
    act_template_id = SelectField(choices=[])
    image_code = StringField()
    code = StringField()
    is_to_rent = BooleanField()
    is_to_sell = BooleanField()
    is_sold = BooleanField()


class PropertyForm(Form):
    title = StringField()
    code = StringField()


class OptionForm(Form):
    title = StringField()
    code = StringField()


class BookingForm(Form):
    item_id = SelectField(choices=[])
    start = StringField()
    end = StringField()


class BookingAdminForm(Form):
    item_id = SelectField(choices=[])
    invoice_id = IntegerField()
    start = StringField()
    end = StringField()


class InvoiceAdminForm(Form):
    is_owner = BooleanField()
    is_payed = BooleanField()
    is_trilife = BooleanField()
    is_deposit = BooleanField()
    person_lastname = StringField()
    person_firstname = StringField()
    person_email = StringField(validators=[Email(message=u'Не верный формат email')])
    person_phone = StringField()
    comments = StringField(widget=TextArea())


class InvoiceForm(Form):
    person_lastname = StringField()
    person_firstname = StringField()
    person_email = StringField(validators=[validators.email(message=u'Проверьте email')])
    person_phone = StringField()
    is_trilife = BooleanField()
    # is_agreement = BooleanField(
    #     validators=[validators.required(message=u'Для продолжения необходимо согласие с условиями')])
    shipping_price = IntegerField()
    # is_payed = BooleanField()


class ContactForm(Form):
    person_name = StringField()
    person_phone = StringField()
    person_email = StringField(validators=[validators.email(message=u'Проверьте email')])
    description = StringField(widget=TextArea())
    image = StringField()


class AddressForm(Form):
    title = StringField()
    address = StringField(widget=TextArea())
    city_title = StringField()
    city_id = SelectField(choices=[('34', 'Spain'), ('971', 'United Arab Emirates'), ('7', 'Russia')])
