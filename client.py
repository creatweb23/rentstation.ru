# -*- coding: utf-8 -*-
from datetime import date, datetime
from flask import request, render_template, redirect, url_for, jsonify, make_response, session
from flask.ext.mail import Message
from dateutil.parser import parse
from pony.orm import db_session, commit
from string import lower
from werkzeug.exceptions import NotFound, BadRequest
from admin import upload_image
from model import BagManager, Invoice, Bag, BookingForm, check_consistency, check_min_period, Booking, InvoiceForm, \
    ContactForm, CategoryManager, Category, PropertyManager, OptionManager, AddressManager, Address
from robokassa import RobokassaForm
from os.path import join
from server import app, _jinja2_filter_datetime, mail
from dateutil.relativedelta import relativedelta
from utils import mail_payed, mail_confirmed
from utils import mail_notify
from yandexkassa import YandexKassaForm


@app.route('/')
@db_session
def index():
    return catalog()


@app.route('/rent/top')
@db_session
def rent_top():
    return render_template('catalog/catalog.html', items=[a for a, b in BagManager.top10(city_id=session.get('city_id'))])


@app.route('/rent/new')
@db_session
def rent_new():
    return render_template('catalog/catalog.html', items=BagManager.find_new(city_id=session.get('city_id')))


@app.route('/catalog')
@db_session
def catalog():
    return render_template('catalog/catalog.html', items=BagManager.find(city_id=session.get('city_id')))


@app.route('/catalog/<int:category_id>')
@db_session
def catalog_category(category_id):
    category = Category.get(id=category_id)
    if not category:
        raise NotFound()

    if category_id in (10, 18):
        oids = request.values.getlist('o[]', None)
        options = []
        if oids:
            options = OptionManager.find(ids=map(int, oids))
        if request.is_xhr:
            return render_template('catalog/catalog_xhr.html',
                                   items=BagManager.find(category=category, options=options))
        else:
            return render_template('catalog/catalog_w_filter.html',
                                   properties=PropertyManager.find_for_category(category),
                                   items=BagManager.find(category=category, city_id=session.get('city_id')),
                                   category_list=CategoryManager.find(),
                                   category_id=category_id,
                                   category_page_title=category.page_title)
    else:
        return render_template('catalog/catalog.html',
                               items=BagManager.find(category=category, city_id=session.get('city_id')),
                               category_list=CategoryManager.find(),
                               category_id=category_id,
                               category_page_title=category.page_title)


@app.route('/catalog/<string:slug>')
@db_session
def catalog_slug_category(slug):
    category = Category.get(slug=slug)
    if not category:
        raise NotFound()

    return catalog_category(category.id)


@app.route('/cart/info')
@db_session
def cart_info():
    invoice_id = request.cookies.get('invoice_id', 0)
    invoice = Invoice.get(id=invoice_id)
    if invoice:
        return jsonify(to_dict(invoice))
    return ''


def to_dict(obj, classkey=None):
    if isinstance(obj, dict):
        data = {}
        for (k, v) in obj.items():
            data[k] = to_dict(v, classkey)
        return data
    elif hasattr(obj, 'to_dict'):
        return to_dict(obj.to_dict())
    elif hasattr(obj, "_ast"):
        return to_dict(obj._ast())
    elif hasattr(obj, "__iter__"):
        return [to_dict(v, classkey) for v in obj]
    elif isinstance(obj, date):
        return _jinja2_filter_datetime(obj)
    else:
        return obj


@app.route('/item/<int:item_id>')
@db_session
def item_resource(item_id):
    item = Bag.get(id=item_id)
    if not item:
        raise NotFound()
    if request.is_xhr:
        return jsonify(to_dict(item))
    return render_template('catalog/item.html', item=item)


@app.route('/item/<string:slug>')
@db_session
def item_slug_resource(slug):
    item = Bag.get(slug=slug)
    if not item:
        raise NotFound()
    if request.is_xhr:
        return jsonify(to_dict(item))
    return render_template('catalog/item.html', item=item)


@app.route('/invoice/<string:uid>')
@db_session
def invoice(uid):
    invoice = Invoice.get(uid=uid)
    if not invoice:
        resp = make_response(redirect('index'))
        return resp
    if invoice and invoice.is_payed:
        resp = make_response(redirect('index'))
        return resp

    form = YandexKassaForm(
        shopId=app.config['KASSA_SHOP_ID'],
        scid=app.config['KASSA_SC_ID'],
        sum=invoice.price,
        customerNumber=invoice.id,
        orderNumber=invoice.id,
        cps_phone=invoice.person_phone,
        cps_email=invoice.person_email,
    )
    return render_template('kassa_confirm.html',
                           invoice=invoice,
                           form=form)


@app.route('/confirm')
@db_session
def confirm():
    invoice_id = request.cookies.get('invoice_id', 0)
    invoice = Invoice.get(id=invoice_id)
    if not invoice:
        resp = make_response(redirect('index'))
        resp.set_cookie('invoice_id', '', expires=0)
        return resp
        # raise NotFound()
    if invoice and invoice.is_payed:
        resp = make_response(redirect('index'))
        resp.set_cookie('invoice_id', '', expires=0)
        return resp

    # invoice.confirm_price = invoice.price
    for booking in invoice.booking:
        booking.confirm_price = booking.price
    commit()

    msg = mail_confirmed(invoice.person_email, invoice)
    mail.send(msg)

    form = RobokassaForm(data={
        'MrchLogin': app.config['ROBOKASSA_LOGIN'],
        'OutSum': invoice.price,
        'InvId': invoice.id,
        'Desc': invoice.description,
        'Email': invoice.person_email,
        'Culture': 'ru'
    })
    return render_template('confirm.html',
                           invoice=invoice,
                           form=form,
                           signature=invoice.get_signature_request(app.config['ROBOKASSA_LOGIN'],
                                                                   app.config.get('ROBOKASSA_PASSWORD1')))


@app.route('/kassa/confirm')
@db_session
def kassa_confirm():
    invoice_id = request.cookies.get('invoice_id', 0)
    invoice = Invoice.get(id=invoice_id)
    if not invoice:
        raise NotFound()
    if invoice and invoice.is_payed:
        resp = make_response(redirect('index'))
        resp.set_cookie('invoice_id', '', expires=0)
        return resp

    for booking in invoice.booking:
        booking.confirm_price = booking.price
    commit()

    msg = mail_confirmed(invoice.person_email, invoice)
    mail.send(msg)

    form = YandexKassaForm(
        shopId=app.config['KASSA_SHOP_ID'],
        scid=app.config['KASSA_SC_ID'],
        sum=invoice.price,
        customerNumber=invoice.id,
        orderNumber=invoice.id,
        cps_phone=invoice.person_phone,
        cps_email=invoice.person_email,
    )
    return render_template('kassa_confirm.html',
                           invoice=invoice,
                           form=form)


@app.route('/book', methods=['POST'])
@db_session
def book():
    item_id = request.values.get('item_id', None)
    invoice_id = request.cookies.get('invoice_id', 0)
    invoice = Invoice() if invoice_id is 0 else Invoice.get(id=invoice_id)
    if not invoice or (invoice and invoice.is_payed):
        invoice = Invoice()

    item = Bag.get(id=item_id)
    if item.is_sold:
        return '', 400

    form = BookingForm(request.form)
    form.item_id.choices = [(str(i.id), i.title) for i in BagManager.find()]
    if request.form and form.validate():
        start = parse(form.start.data)
        end = parse(form.end.data)
        if start > end:
            start, end = end, start
        if start > (datetime.now() + relativedelta(years=1)) or end > (datetime.now() + relativedelta(years=1)):
            form.start.errors.append(u'В указанный период товар недоступен')
        elif not check_consistency(item, start, end):
            form.start.errors.append(u'В указанный период товар недоступен')
        elif not check_min_period(min_period=item.min_period, start=start, end=end):
            form.start.errors.append(
                u'Минимальный период аренды товара - %s' % (u'одна неделя' if item.min_period == 1 else u'один месяц'))
        else:
            Booking(item=item,
                    invoice=invoice,
                    start=start,
                    end=end)
            commit()
            resp = make_response(jsonify(to_dict(invoice)))
            resp.set_cookie('invoice_id', str(invoice.id))
            return resp
    return u". ".join(sum(form.errors.values(), [])), 400


@app.route('/book_delete/<int:booking_id>')
@db_session
def book_delete(booking_id):
    invoice_id = request.cookies.get('invoice_id', 0)
    invoice = Invoice() if invoice_id is 0 else Invoice.get(id=invoice_id)
    if invoice and invoice.is_payed:
        raise BadRequest()
    booking = Booking.get(invoice=invoice, id=booking_id)
    if booking:
        booking.delete()
    return redirect('cart')


@app.route('/cart', methods=['GET', 'POST'])
@db_session
def cart():
    invoice_id = request.cookies.get('invoice_id', 0)
    invoice = Invoice.get(id=invoice_id)
    form = InvoiceForm(request.form, data=to_dict(invoice))
    remove_cookie = False

    if invoice and invoice.is_payed:
        remove_cookie = True
        invoice = None

    if request.form and form.validate():
        shipping_price = 3000 if form.shipping_price.data == 1 else 0
        # price = sum([b.price for b in invoice.booking]) / (2 if form.is_trilife.data else 1) + shipping_price
        invoice.person_lastname = form.person_lastname.data
        invoice.person_firstname = form.person_firstname.data
        invoice.person_email = form.person_email.data
        invoice.person_phone = form.person_phone.data
        # invoice.is_trilife = form.is_trilife.data
        # invoice.price = price
        invoice.shipping_price = shipping_price
        commit()
        return redirect(url_for('kassa_confirm'))

    resp = make_response(render_template('cart.html', invoice=invoice, form=form))
    if remove_cookie:
        resp.set_cookie('invoice_id', '', expires=0)
    return resp


@app.route('/success')
@db_session
def success():
    return render_template('success.html')


def send_contact(form, attach=None):
    msg = Message('Сообщение с RentStation', recipients=['info@rentstation.ru'], reply_to=form.person_email.data)
    msg.sender = ('RentStation', 'info@rentstation.ru')
    msg.body = render_template('email/contact.txt', form=form)
    if attach:
        with app.open_resource(attach) as fp:
            msg.attach('image.png', "image/png", fp.read())
    mail.send(msg)


@app.route('/robokassa/proceed')
@db_session
def robokassa_proceed():
    out_sum = request.values.get('OutSum', None)
    inv_id = request.values.get('InvId', None)
    signature = request.values.get('SignatureValue', None)
    invoice = Invoice.get(id=inv_id)
    if not invoice:
        raise NotFound()
    if float(invoice.price) == float(out_sum) and \
                    lower(signature) == lower(
                    invoice.get_signature_responce(app.config['ROBOKASSA_PASSWORD2'], out_sum)):
        if not invoice.is_payed:
            invoice.is_payed = True
            commit()
            for msg in [mail_payed(invoice.person_email, invoice), mail_notify(invoice)]:
                mail.send(msg)
        return 'OK%s' % invoice.id
    else:
        return 'ERROR', 500


@app.route('/robokassa/success')
@db_session
def robokassa_success():
    out_sum = request.values.get('OutSum', None)
    inv_id = request.values.get('InvId', None)
    if not inv_id:
        raise NotFound()
    signature = request.values.get('SignatureValue', None)
    invoice = Invoice.get(id=inv_id)
    if not invoice:
        raise NotFound()
    # if float(invoice.price) == float(out_sum) and \
    #                 lower(signature) == lower(
    #                 invoice.get_signature_responce(app.config['ROBOKASSA_PASSWORD1'], out_sum)):
    if not invoice.is_payed:
        invoice.is_payed = True
        commit()
        for msg in [mail_payed(invoice.person_email, invoice), mail_notify(invoice)]:
            mail.send(msg)
    return render_template('success.html')
    # else:
    #     return 'ERROR', 500


@app.route('/robokassa/fail')
@db_session
def robokassa_fail():
    return render_template('fail.html')


@app.route('/address')
@db_session
def address():
    msk_address = sorted(AddressManager.find(city_id=1), key=lambda x: x.id)
    spb_address = sorted(AddressManager.find(city_id=2), key=lambda x: x.id)
    sev_address = sorted(AddressManager.find(city_id=3), key=lambda x: x.id)
    return render_template('address.html', msk_address=msk_address, spb_address=spb_address, sev_address=sev_address)


@app.route('/address/<int:address_id>')
@db_session
def address_item(address_id):
    return render_template('address_item.html', addr=Address.get(id=address_id))


@app.route('/contact', methods=['GET', 'POST'])
def contact():
    form = ContactForm(request.form)
    is_sent = False
    if request.form and form.validate():
        image_attach = None
        if request.files['image']:
            image_name = upload_image(request.files['image'])
            image_attach = join(app.config['UPLOAD_FOLDER'], image_name)
        send_contact(form, attach=image_attach)
        is_sent = True
    return render_template('contact.html', form=form, is_sent=is_sent)


@app.route('/shop', methods=['GET'])
@db_session
def shop():
    return render_template('catalog/shop.html',
                           items=BagManager.shop(city_id=session.get('city_id')),
                           category_list=CategoryManager.find(),
                           category_id=None)


@app.route('/set_city/<int:city_id>', methods=['GET'])
def set_city(city_id):
    # if city_id == 2:
    #     session['city_title'] = u'Санкт-Петербург'
    #     session['city_id'] = 2
    # elif city_id == 3:
    #     session['city_title'] = u'Севастополь'
    #     session['city_id'] = 3
    # else:
    #     session['city_title'] = u'Москва'
    #     session['city_id'] = 1
    session['city_id'] = None
    session['city_title'] = None
    return redirect(url_for('index'))


@app.route('/blog', methods=['GET'])
def articles():
    return render_template('articles.html')


@app.route('/blog/nechego-nadet-part1', methods=['GET'])
@db_session
def articles_nechego_nadet():
    category = Category.get(slug='road-bikes')
    return render_template('nechego-nadet.html',
                           items=BagManager.find(category=category, is_sold=False))


@app.route('/blog/nechego-nadet-part2', methods=['GET'])
@db_session
def articles_nechego_nadet2():
    category = Category.get(slug='road-bikes')
    return render_template('nechego-nadet2.html',
                           items=BagManager.find(category=category, is_sold=False))

if __name__ == '__main__':
    app.run()
